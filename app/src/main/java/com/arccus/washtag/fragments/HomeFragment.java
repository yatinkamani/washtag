package com.arccus.washtag.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.arccus.washtag.R;
import com.arccus.washtag.activity.CarWashActivity;
import com.arccus.washtag.activity.DryCleanPricingActivity;
import com.arccus.washtag.activity.HospitalWashActivity;
import com.arccus.washtag.activity.HostelWashActivity;
import com.arccus.washtag.activity.HotelWashActivity;
import com.arccus.washtag.activity.WashIronActivity;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import me.everything.android.ui.overscroll.VerticalOverScrollBounceEffectDecorator;
import me.everything.android.ui.overscroll.adapters.ScrollViewOverScrollDecorAdapter;

public class HomeFragment extends Fragment implements View.OnClickListener {

    public LinearLayout llDryClean, llWashIron, llCarWash, llHotelWash, llHospitalWash, llHostelWash, llStreamPress, llWashPress;
    private OnFragmentInteractionListener mListener;

    private TextView txt_washFoldSteam;

    private ScrollView scrollView;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        llDryClean = (LinearLayout) view.findViewById(R.id.llDryClean);
        llWashIron = (LinearLayout) view.findViewById(R.id.llWashIron);
        llCarWash = (LinearLayout) view.findViewById(R.id.llCarWash);
        llHotelWash = (LinearLayout) view.findViewById(R.id.llHotelWash);
        llHospitalWash = (LinearLayout) view.findViewById(R.id.llHospitalWash);
        llHostelWash = (LinearLayout) view.findViewById(R.id.llHostelWash);
        llStreamPress = (LinearLayout) view.findViewById(R.id.llStreamPress);
        llWashPress = (LinearLayout) view.findViewById(R.id.llWashPress);
        scrollView = (ScrollView) view.findViewById(R.id.scrollView);
        new VerticalOverScrollBounceEffectDecorator(new ScrollViewOverScrollDecorAdapter(scrollView));

//        OverScrollDecoratorHelper.setUpOverScroll(scrollView);

        txt_washFoldSteam = (TextView) view.findViewById(R.id.txt_washFoldSteam);

        llDryClean.setOnClickListener(this);
        llWashIron.setOnClickListener(this);
        llCarWash.setOnClickListener(this);
        llHotelWash.setOnClickListener(this);
        llHospitalWash.setOnClickListener(this);
        llHostelWash.setOnClickListener(this);
        llStreamPress.setOnClickListener(this);
        llWashPress.setOnClickListener(this);

        txt_washFoldSteam.setSelected(true);
        txt_washFoldSteam.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        txt_washFoldSteam.setMarqueeRepeatLimit(2);

        return view;
    }

    @Override
    public void onClick(View view) {
        if (view == llWashIron) {
            setLinCardElevation(llWashIron, 20f,getResources().getColor(R.color.home_item_color_dark));
            Intent washIron_intent = new Intent(getActivity(), WashIronActivity.class);
            washIron_intent.putExtra("package_category", "wash & fold");
            startActivity(washIron_intent);
        } else if (view == llDryClean) {
            setLinCardElevation(llDryClean, 20f,getResources().getColor(R.color.home_item_color_dark));
            Intent intent = new Intent(getActivity(), DryCleanPricingActivity.class);
            startActivity(intent);
        } else if (view == llStreamPress) {
            setLinCardElevation(llStreamPress, 20f,getResources().getColor(R.color.home_item_color_dark));
            Intent washIron_intent = new Intent(getActivity(), WashIronActivity.class);
            washIron_intent.putExtra("package_category", "steam press");
            startActivity(washIron_intent);
        } else if (view == llWashPress) {
            setLinCardElevation(llWashPress, 20f,getResources().getColor(R.color.home_item_color_dark));
            Intent intent = new Intent(getActivity(), WashIronActivity.class);
            intent.putExtra("package_category", "wash & press");
            startActivity(intent);
        } else if (view == llCarWash) {
            setLinCardElevation(llCarWash, 20f,getResources().getColor(R.color.home_item_color_dark));
            Intent intent = new Intent(getActivity(), CarWashActivity.class);
            startActivity(intent);
        } else if (view == llHotelWash) {
            setLinCardElevation(llHotelWash, 20f,getResources().getColor(R.color.home_item_color_dark));
            Intent intent = new Intent(getActivity(), HotelWashActivity.class);
            startActivity(intent);
        } else if (view == llHostelWash) {
            setLinCardElevation(llHostelWash, 20f,getResources().getColor(R.color.home_item_color_dark));
            Intent intent = new Intent(getActivity(), HostelWashActivity.class);
            startActivity(intent);
        } else if (view == llHospitalWash) {
            setLinCardElevation(llHospitalWash, 20f, getResources().getColor(R.color.home_item_color_dark));
            Intent intent = new Intent(getActivity(), HospitalWashActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setLinCardElevation(llWashIron, 6f, getResources().getColor(R.color.home_item_color));
        setLinCardElevation(llDryClean, 6f, getResources().getColor(R.color.home_item_color));
        setLinCardElevation(llCarWash, 6f, getResources().getColor(R.color.home_item_color));
        setLinCardElevation(llHotelWash, 6f, getResources().getColor(R.color.home_item_color));
        setLinCardElevation(llHospitalWash, 6f, getResources().getColor(R.color.home_item_color));
        setLinCardElevation(llHostelWash, 6f, getResources().getColor(R.color.home_item_color));
        setLinCardElevation(llWashPress, 6f, getResources().getColor(R.color.home_item_color));
        setLinCardElevation(llStreamPress, 6f, getResources().getColor(R.color.home_item_color));
    }

    interface OnFragmentInteractionListener {

    }

    private void setLinCardElevation(LinearLayout layout, float elevation, int color) {
        CardView cv = (CardView) layout.getParent();
        cv.setCardElevation(elevation);
        cv.setCardBackgroundColor(color);
    }

}
