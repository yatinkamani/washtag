package com.arccus.washtag.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arccus.washtag.R;

public class HoseHoldFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    public HoseHoldFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.e("HOUSEHOLD","True");
        return inflater.inflate(R.layout.fragment_hose_hold, container, false);
    }

    private interface OnFragmentInteractionListener {

    }
}
