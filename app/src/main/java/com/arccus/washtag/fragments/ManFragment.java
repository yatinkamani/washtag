package com.arccus.washtag.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arccus.washtag.R;

public class ManFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    public ManFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.e("MAN","True");
        return inflater.inflate(R.layout.fragment_man, container, false);
    }

    private interface OnFragmentInteractionListener {
        
    }
}
