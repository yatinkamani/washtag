package com.arccus.washtag.utils;

import android.widget.EditText;

import java.util.regex.Pattern;

public class Validate
{

	/* return true if edit box is not empty otherwise return false */

    public boolean checkEmpty(EditText edit) {
        String str = edit.getText().toString().trim();
        return str.length() != 0;
    }


	/* return true if edit box is contain spaces otherwise return false */

    public boolean checkSpaces(EditText edit) {
        String str = edit.getText().toString().trim();
        return str.contains(" ");
    }

	/* return true if email is valid otherwise return false */
    public boolean checkForEmail(EditText edit) {
        String str = edit.getText().toString();
        return android.util.Patterns.EMAIL_ADDRESS.matcher(str).matches();
    }

	/* return true if website is valid otherwise return false */

    public boolean checkForWebsite(EditText edit, String editName) {
        String str = edit.getText().toString();
        if (android.util.Patterns.DOMAIN_NAME.matcher(str).matches()) {

            return true;
        }
        //toastMsg(editName + " "
        //+ _context.getString(R.string.is_not_valid).toString());
        return false;
    }


    public static boolean checkForPhone(String str) {
//        String str = edit.getText().toString();
        return android.util.Patterns.PHONE.matcher(str).matches();
    }


    /* return true if length is invalid otherwise return false */
    public boolean checkForLength(EditText edit) {
        String str = edit.getText().toString().trim();
        return str.length() == 10;
    }

    /* return true if integer is present otherwise return false */
    public boolean checkForInteger(EditText edit) {
        String str = edit.getText().toString().trim();
        return str.matches(".*\\d.*");
    }

    public boolean checkSpecialChar(EditText edit) {
        String str = edit.getText().toString().trim();
        if (!str.matches("[a-zA-Z.? ]*")) {
            //toastMsg(editName + " "
            //	+ _context.getString(R.string.is_not_valid).toString());
            return true;
        }
        return false;
    }

    public static boolean isValidMobile(String phone) {
        boolean check=false;
        if(Pattern.matches(".*\\d.*", phone)) {
            //if(phone.length() >= 10 && phone.length() <= 15) {
                 if(phone.length() == 10) {
                check = true;

            } else {
                check = false;
            }
        } else {
            check=false;
        }
        return check;
    }

}