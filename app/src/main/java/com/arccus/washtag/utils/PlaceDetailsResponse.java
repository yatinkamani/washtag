package com.arccus.washtag.utils;

import java.util.Map;

/**
 * Created by Kaprat on 26-10-2018.
 */

public interface PlaceDetailsResponse {
    public void onPlaceDetailSuccess(boolean isSuccess, Map map);
}