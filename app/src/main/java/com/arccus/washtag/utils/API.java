package com.arccus.washtag.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class API {

    private Context context;
    private APIResponse apiResponse = null;
    private ProgressDialog prd;

    public API(Context context, APIResponse apiResponse) {
        this.context = context;
        this.apiResponse = apiResponse;
    }

    public void execute(final int requestCode, final String url, final Map<String, String> bodyParameter, final boolean showLoading) {
        if (showLoading)
            showLoader();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (showLoading)
                    dismissLoader();

                if (apiResponse != null)
                    apiResponse.onAPISuccess(requestCode,true, response);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        if (showLoading)
                            dismissLoader();

                        String message = null;
                        if (volleyError instanceof NetworkError) {
                            message = "Please check your network connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "Please try again after some time!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Unauthorized access";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof NoConnectionError) {
                            message = "Please check your connection!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Oops. Connection Timeout!";
                        }

                        if (apiResponse != null)
                            apiResponse.onAPIError(requestCode,true, message);
                        volleyError.printStackTrace();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                Log.e("API URL : ", String.format("%s", url));
                if(bodyParameter != null) {
                    for (Map.Entry<String, String> entry : bodyParameter.entrySet()) {
                        String key = entry.getKey();
                        String value = entry.getValue();
                        params.put(key, value);
                        Log.e("Params (Body) : ", String.format("%s:%s", key, value));
                    }
                }

                return params;
            }

          /*  @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    private void showLoader() {
        prd = new ProgressDialog(context);
        prd.setMessage("Please, Wait a moment");
        prd.setTitle("Processing...");
        prd.show();
    }

    private void dismissLoader() {
        if (prd != null && prd.isShowing())
            prd.dismiss();
    }

}
