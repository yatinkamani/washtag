package com.arccus.washtag.utils;



public interface APIResponse {
    public void onAPISuccess(int requestCode, boolean isSuccess, String response);
    public void onAPIError(int requestCode, boolean isError, String error);
}
