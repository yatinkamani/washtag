package com.arccus.washtag.utils;

/**
 * Created by Karat on 14-10-2018.
 */

public class Services {
//    public static final String BASE_URL = "http://192.168.1.106/pick/api/";
//    public static final String BASE_URL = "http://pickupmylaundry.in/api/";

    public static final String api_key = "AIzaSyCR1Se2r7aahAmuVxxr5pDTr1al8eswwW0";

    public static final String BASE_URL = "http://kaprat.com/dev/washtag/api/";
    public static final String LOGIN = BASE_URL + "login";
    public static final String REGISTER = BASE_URL + "registration";
    public static final String SEND_OTP = BASE_URL + "send_otp";
    public static final String ADD_LOCATION = BASE_URL + "add_address";
    public static final String FORGOT_PASSWORD = BASE_URL + "forgotpassword";

    public static final String GET_ORDER = BASE_URL + "get_order";
    public static final String GET_ORDER_DETAIL = BASE_URL + "get_order_detail";
    public static final String GET_PACKAGE_INFO = BASE_URL + "active_package_info";
    public static final String BUY_PACKAGE = BASE_URL + "buy_package";
    public static final String PACKAGE_LIST = BASE_URL + "packages";
    public static final String DRY_CLEANING = BASE_URL + "dry_cleaning";
    public static final String PACKAGE_DETAIL = BASE_URL + "packages_details";
    public static final String EDIT_PROFILE = BASE_URL + "edit_profile";
    public static final String NO_NETWORK = "No Internet Connection";

}
