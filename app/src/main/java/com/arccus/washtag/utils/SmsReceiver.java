package com.arccus.washtag.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.arccus.washtag.helper.SimsLister;


/**
 * Created by Kaprat on 02-11-2018.
 */

public class SmsReceiver extends BroadcastReceiver {
    private static SimsLister mListener;
    Boolean b = true;
    String ABC;

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.e("RECEIVER", "TRUE");

        Bundle data = intent.getExtras();
        Object[] pDus = (Object[]) data.get("pdus");
        for (int i = 0; i < pDus.length; i++) {
            SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pDus[i]);
            String sender = smsMessage.getDisplayOriginatingAddress();
            // b=sender.endsWith("WNRCRP");  //Just to fetch otp sent from WNRCRP
            String messageBody = smsMessage.getMessageBody();
            ABC = messageBody.replaceAll("[^0-9]", "");   // here ABC contains otp
            // which is in number format
            //Pass on the text to our listener.
            if (b) {
                Log.e("Tag BOOLEAN", " " + ABC);
                mListener.messageReceived(ABC);  // attach value to interface object
            }
            Log.e("Tag BOOLEAN", " " + ABC);
        }
    }

    public static void bindListener(SimsLister listener) {
        mListener = listener;
    }
}
