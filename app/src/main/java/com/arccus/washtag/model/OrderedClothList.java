package com.arccus.washtag.model;

/*
 * Created by KCS on 22-Nov-18.
 * Email : info.kaprat@gmail.com
 */
public class OrderedClothList {

    public String getCloth_name() {
        return cloth_name;
    }

    public void setCloth_name(String cloth_name) {
        this.cloth_name = cloth_name;
    }

    public String getCloth_quantity() {
        return cloth_quantity;
    }

    public void setCloth_quantity(String cloth_quantity) {
        this.cloth_quantity = cloth_quantity;
    }

    private String cloth_name, cloth_quantity;
}
