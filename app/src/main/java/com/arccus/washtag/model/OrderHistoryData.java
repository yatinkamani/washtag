package com.arccus.washtag.model;

/*
 * Created by KCS on 22-Nov-18.
 * Email : info.kaprat@gmail.com
 */
public class OrderHistoryData {

    private String order_no;

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    private String order_date;
}
