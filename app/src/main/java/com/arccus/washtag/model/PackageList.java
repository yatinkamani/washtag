package com.arccus.washtag.model;

/*
 * Created by KCS on 20-Nov-18.
 * Email : info.kaprat@gmail.com
 */
public class PackageList {

    private String package_plan_id;
    private String package_person;
    private String package_period;
    private String package_id;
    private String package_price;
    private String discount_price;

    public String getCloth_limit() {
        return cloth_limit;
    }

    public void setCloth_limit(String cloth_limit) {
        this.cloth_limit = cloth_limit;
    }

    private String cloth_limit;

    public String getPackage_id() {
        return package_id;
    }

    public void setPackage_id(String package_id) {
        this.package_id = package_id;
    }

    public String getPackage_price() {
        return package_price;
    }

    public void setPackage_price(String package_price) {
        this.package_price = package_price;
    }

    public String getDiscount_price() {
        return discount_price;
    }

    public void setDiscount_price(String discount_price) {
        this.discount_price = discount_price;
    }

    public String getFinal_price() {
        return final_price;
    }

    public void setFinal_price(String final_price) {
        this.final_price = final_price;
    }

    private String final_price;

    public String getPackage_plan_id() {
        return package_plan_id;
    }

    public void setPackage_plan_id(String package_plan_id) {
        this.package_plan_id = package_plan_id;
    }

    public String getPackage_person() {
        return package_person;
    }

    public void setPackage_person(String package_person) {
        this.package_person = package_person;
    }

    public String getPackage_period() {
        return package_period;
    }

    public void setPackage_period(String package_period) {
        this.package_period = package_period;
    }
}
