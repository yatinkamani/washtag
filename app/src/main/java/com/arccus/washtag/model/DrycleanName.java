package com.arccus.washtag.model;

import java.util.ArrayList;

/*
 * Created by KCS on 25-Dec-18.
 * Email : info.kaprat@gmail.com
 */
public class DrycleanName {

    private String dryclean_id;

    public ArrayList<DrycleanList> getDrycleanListArray() {
        return drycleanListArray;
    }

    public void setDrycleanListArray(ArrayList<DrycleanList> drycleanListArray) {
        this.drycleanListArray = drycleanListArray;
    }

    private ArrayList<DrycleanList> drycleanListArray;

    public String getDryclean_id() {
        return dryclean_id;
    }

    public void setDryclean_id(String dryclean_id) {
        this.dryclean_id = dryclean_id;
    }

    public String getDryclean_name() {
        return dryclean_name;
    }

    public void setDryclean_name(String dryclean_name) {
        this.dryclean_name = dryclean_name;
    }

    private String dryclean_name;
}
