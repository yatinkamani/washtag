package com.arccus.washtag.model;

public class CarType {

    private String carType_id, carType_name;

    public String getCarType_id() {
        return carType_id;
    }

    public void setCarType_id(String carType_id) {
        this.carType_id = carType_id;
    }

    public String getCarType_name() {
        return carType_name;
    }

    public void setCarType_name(String carType_name) {
        this.carType_name = carType_name;
    }

    @Override
    public String toString() {
        return carType_name ;
    }
}
