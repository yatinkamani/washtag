package com.arccus.washtag.model;

import java.util.ArrayList;

/*
 * Created by KCS on 24-Dec-18.
 * Email : info.kaprat@gmail.com
 */
public class PackageName {

    private String package_name;

    public ArrayList<PackageList> getPackageListArray() {
        return packageListArray;
    }

    public void setPackageListArray(ArrayList<PackageList> packageListArray) {
        this.packageListArray = packageListArray;
    }

    private ArrayList<PackageList> packageListArray;

    public String getPackage_name() {
        return package_name;
    }

    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }

    public String getPackage_id() {
        return package_id;
    }

    public void setPackage_id(String package_id) {
        this.package_id = package_id;
    }

    private String package_id;

}
