package com.arccus.washtag.model;

/**
 * Created by Kaprat on 30-10-2018.
 */

public class ClothDataList {

    private String clothCategory;
    private String clothName;

    public String getClothCategory() {
        return clothCategory;
    }

    public void setClothCategory(String clothCategory) {
        this.clothCategory = clothCategory;
    }

    public String getClothName() {
        return clothName;
    }

    public void setClothName(String clothName) {
        this.clothName = clothName;
    }

    public String getClothUri() {
        return clothUri;
    }

    public void setClothUri(String clothUri) {
        this.clothUri = clothUri;
    }

    private String clothUri;
}
