package com.arccus.washtag.model;

/*
 * Created by KCS on 24-Dec-18.
 * Email : info.kaprat@gmail.com
 */
public class DrycleanList {

    private String cloth_name;

    private String cloth_id;

    private String garment_category_id;

    private String cloth_price;

    public String getCloth_id() {
        return cloth_id;
    }

    public void setCloth_id(String cloth_id) {
        this.cloth_id = cloth_id;
    }

    public String getGarment_category_id() {
        return garment_category_id;
    }

    public void setGarment_category_id(String garment_category_id) {
        this.garment_category_id = garment_category_id;
    }

    public String getCloth_name() {
        return cloth_name;
    }

    public void setCloth_name(String cloth_name) {
        this.cloth_name = cloth_name;
    }

    public String getCloth_price() {
        return cloth_price;
    }

    public void setCloth_price(String cloth_price) {
        this.cloth_price = cloth_price;
    }
}
