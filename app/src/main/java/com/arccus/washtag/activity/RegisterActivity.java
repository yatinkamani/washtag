package com.arccus.washtag.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.arccus.washtag.R;
import com.arccus.washtag.utils.API;
import com.arccus.washtag.utils.APIResponse;
import com.arccus.washtag.utils.Helper;
import com.arccus.washtag.utils.Services;
import com.arccus.washtag.utils.Validate;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {

    private EditText etUserName, etEmail, etPhoneNo, etPassword, etConfirmPassword;

    LinearLayout llRegister, llTerms;
    Animation animMove;

    String userName, email, phoneNo, password, confirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initViews();
    }

    private void initViews() {

        etUserName = (EditText) findViewById(R.id.etUserName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPhoneNo = (EditText) findViewById(R.id.etPhoneNo);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etConfirmPassword = (EditText) findViewById(R.id.etConfirmPassword);

        llTerms = (LinearLayout) findViewById(R.id.llTerms);
        llRegister = (LinearLayout) findViewById(R.id.llRegister);
        animMove = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move);

        llRegister.startAnimation(animMove);

    }

    public void onRegister(View view) {

        userName = etUserName.getText().toString().trim();
        email = etEmail.getText().toString().trim();
        phoneNo = etPhoneNo.getText().toString().trim();
        password = etPassword.getText().toString();
        confirmPassword = etConfirmPassword.getText().toString().trim();

        if (TextUtils.isEmpty(userName)) {
            Helper.showToast(RegisterActivity.this, "Enter user name");
        } else if (TextUtils.isEmpty(email)) {
            Helper.showToast(RegisterActivity.this, "Enter email address");
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Helper.showToast(RegisterActivity.this, "Enter valid email address");
        } else if (TextUtils.isEmpty(phoneNo)) {
            Helper.showToast(RegisterActivity.this, "Enter phone no.");
        } else if (!Validate.isValidMobile(phoneNo)) {
            Helper.showToast(RegisterActivity.this, "Enter valid phone no.");
        } else if (TextUtils.isEmpty(password)) {
            Helper.showToast(RegisterActivity.this, "Enter password");
        } else if (TextUtils.isEmpty(confirmPassword)) {
            Helper.showToast(RegisterActivity.this, "Enter confirm password");
        } else if (!password.equals(confirmPassword)) {
            Helper.showToast(RegisterActivity.this, " Confirm password does not match");
        } else {
            if (Helper.isNetworkAvailable(RegisterActivity.this)) {
                APIResponse apiResponse = new APIResponse() {
                    @Override
                    public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                        Helper.showLog("OTP : " + response);
                        try {
                            JSONObject object = new JSONObject(response);
                            String status = object.getString("status");
                            String message = object.getString("message");
                            if (status.equalsIgnoreCase("true")) {

                                JSONObject result = object.getJSONObject("data");
                                String otp = result.getString("otp");

                                SharedPreferences sp = getApplicationContext().getSharedPreferences("Laundry", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sp.edit();
                                editor.putString("user_name", userName);
                                editor.putString("email", email);
                                editor.putString("phone_no", phoneNo);
                                editor.putString("password", password);
                                editor.apply();

                                Helper.showToast(RegisterActivity.this, message);

                                Intent intent = new Intent(RegisterActivity.this, OtpVerificationActivity.class);
                                intent.putExtra("From", "registration");
                                intent.putExtra("OTP", otp);
                                startActivity(intent);
                                //  intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                //  finish();

                            } else {
                                Helper.showToast(RegisterActivity.this, message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onAPIError(int requestCode, boolean isError, String error) {
                        if (isError) {
                            Helper.showToast(RegisterActivity.this, error);
                        }
                    }
                };

                HashMap<String, String> params = new HashMap<>();
                params.put("user_name", userName);
                params.put("email", email);
                params.put("phone_no", phoneNo);

                API api = new API(RegisterActivity.this, apiResponse);
                api.execute(1, Services.SEND_OTP, params, true);
            } else {
                Helper.showToast(RegisterActivity.this, Services.NO_NETWORK);
            }
        }
    }

    public void onTermsConditions(View view) {

        Intent intent = new Intent(RegisterActivity.this, TermsConditionActivity.class);
        startActivity(intent);

    }

}
