package com.arccus.washtag.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.arccus.washtag.R;
import com.arccus.washtag.adapter.PlacesAutoCompleteAdapter;
import com.arccus.washtag.utils.API;
import com.arccus.washtag.utils.APIResponse;
import com.arccus.washtag.utils.Helper;
import com.arccus.washtag.utils.Services;
import com.arccus.washtag.utils.Validate;
import com.arccus.washtag.view.WorkaroundMapFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import me.everything.android.ui.overscroll.VerticalOverScrollBounceEffectDecorator;
import me.everything.android.ui.overscroll.adapters.ScrollViewOverScrollDecorAdapter;

public class EditProfileActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener {

    private ImageView ivBack;
    private EditText etUserName, etEmail, etPhoneNo, etPassword, etLocality;
    private LatLngBounds US_BOUNDS = new LatLngBounds(new LatLng(37.1747682, -97.7619313), new LatLng(39.7747695, 101.4038086));
    private AutoCompleteTextView userAddress;
    private GoogleMap mMap;
    private PlacesAutoCompleteAdapter mPlacesAdapter;
    private LinearLayout llSetting;
    private GoogleApiClient mGoogleApiClient;
    double latitude, longitude;
    Marker map_marker;
    Animation animMove;
    private ScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        /*if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }*/

        scrollView = (ScrollView) findViewById(R.id.scrollView);

        new VerticalOverScrollBounceEffectDecorator(new ScrollViewOverScrollDecorAdapter(scrollView));
        mGoogleApiClient = new GoogleApiClient.Builder(EditProfileActivity.this)
                .enableAutoManage(this,0,this)
                .addApi(Places.GEO_DATA_API)
                .build();

        WorkaroundMapFragment mapFragment = ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapUser));
        mapFragment.getMapAsync(EditProfileActivity.this);

        ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapUser))
                .setListener(new WorkaroundMapFragment.OnTouchListener() {
                    @Override
                    public void onTouch() {
                        scrollView.requestDisallowInterceptTouchEvent(true);
                    }
                });

        userAddress = (AutoCompleteTextView) findViewById(R.id.userAddress);

        mPlacesAdapter = new PlacesAutoCompleteAdapter(EditProfileActivity.this, android.R.layout.simple_list_item_1, mGoogleApiClient, US_BOUNDS, null);
        userAddress.setOnItemClickListener(mAutocompleteClickListener);
        userAddress.setAdapter(mPlacesAdapter);

        initViews();
    }

    private void initViews(){

        etUserName = (EditText) findViewById(R.id.etUserName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPhoneNo = (EditText) findViewById(R.id.etPhoneNo);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etLocality  = (EditText) findViewById(R.id.etLocality);

        llSetting = (LinearLayout) findViewById(R.id.llSetting);
        animMove = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.move);

        ivBack = (ImageView) findViewById(R.id.ivBack);

        SharedPreferences sp = getApplicationContext().getSharedPreferences("Laundry", Context.MODE_PRIVATE);
        String user_name = sp.getString("user_name", "").trim();
        String email = sp.getString("email", "").trim();
        String address = sp.getString("address", "").trim();
        String phone_no = sp.getString("phone_no", "").trim();
        String locality = sp.getString("locality", "").trim();

        etUserName.setText(user_name);
        etEmail.setText(email);
        etPhoneNo.setText(phone_no);

        if(address.equals("null") || address.isEmpty()){
            userAddress.setText("");
        }else {
            userAddress.setText(address);
        }

        if(locality.equals("null") || locality.isEmpty()){
            etLocality.setText("");
        }else {
            etLocality.setText(locality);
        }

        llSetting.startAnimation(animMove);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlacesAutoCompleteAdapter.PlaceAutocomplete item = mPlacesAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            //            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
            //            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

            com.google.android.libraries.places.api.Places.initialize(getApplicationContext(),Services.api_key);
            PlacesClient placesClient = com.google.android.libraries.places.api.Places.createClient(getApplicationContext());
            List<com.google.android.libraries.places.api.model.Place.Field> placeFields = Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.ID,com.google.android.libraries.places.api.model.Place.Field.NAME, com.google.android.libraries.places.api.model.Place.Field.LAT_LNG, com.google.android.libraries.places.api.model.Place.Field.ADDRESS);
            FetchPlaceRequest request = FetchPlaceRequest.builder(placeId, placeFields).build();
            placesClient.fetchPlace(request).addOnSuccessListener(new OnSuccessListener<FetchPlaceResponse>() {
                @Override
                public void onSuccess(FetchPlaceResponse response) {
                    com.google.android.libraries.places.api.model.Place place = response.getPlace();
                    LatLng latLng = place.getLatLng();
                    Helper.showLog("Lat Lang : " + latLng);
                    latitude = latLng.latitude;
                    longitude = latLng.longitude;
                    Helper.showLog("LATITUDE : " + latitude);
                    Helper.showLog("LONGITUDE : " + longitude);
                    if (mMap != null) {
                        map_marker.setPosition(latLng);
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                        mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    if (exception instanceof ApiException) {
                        Toast.makeText(getApplicationContext(), exception.getMessage() + "", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
    };

    /*private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e("PLACE", "Place query did not complete. Error: " +places.getStatus().toString());
                return;
            }

            // Selecting the first object buffer.
            final Place place = places.get(0);
            LatLng latLng = place.getLatLng();
            Helper.showLog("Lat Lang : " + latLng);
            latitude = latLng.latitude;
            longitude = latLng.longitude;
            Helper.showLog("LATITUDE : " + latitude);
            Helper.showLog("LONGITUDE : " + longitude);
            if (mMap != null) {
                map_marker.setPosition(latLng);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
            }
        }
    };*/

    public void onUpdate(View view){

        SharedPreferences sp = getApplicationContext().getSharedPreferences("Laundry", Context.MODE_PRIVATE);
        String user_id = sp.getString("user_id", "").trim();

        String userName = etUserName.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String phoneNo = etPhoneNo.getText().toString().trim();
        String password = etPassword.getText().toString();
        String Locality = etLocality.getText().toString().trim();
        String user_Address = userAddress.getText().toString().trim();
        final String Latitude = String.valueOf(latitude);
        final String Longitude = String.valueOf(longitude);

        if (TextUtils.isEmpty(userName)) {
            Helper.showToast(EditProfileActivity.this, "Enter user name");
        } else if (TextUtils.isEmpty(email)) {
            Helper.showToast(EditProfileActivity.this, "Enter email address");
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Helper.showToast(EditProfileActivity.this, "Enter valid email address");
        } else if(TextUtils.isEmpty(phoneNo)){
            Helper.showToast(EditProfileActivity.this, "Enter phone no.");
        } else if(!Validate.isValidMobile(phoneNo)){
            Helper.showToast(EditProfileActivity.this, "Enter valid phone no.");
        } else if(TextUtils.isEmpty(Locality)){
            Helper.showToast(EditProfileActivity.this, "Enter locality");
        }else if(TextUtils.isEmpty(user_Address)){
            Helper.showToast(EditProfileActivity.this, "Enter address");
        } else if (Latitude.equalsIgnoreCase("0.0") || Longitude.equalsIgnoreCase("0.0")) {
            Helper.showToast(EditProfileActivity.this, "Select Location in MAP");
        }else{
            if (Helper.isNetworkAvailable(EditProfileActivity.this)) {
                APIResponse apiResponse = new APIResponse() {
                    @Override
                    public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                        Helper.showLog("Register : " + response);
                        try {
                            JSONObject object = new JSONObject(response);
                            String status = object.getString("status");
                            String message = object.getString("message");
                            if (status.equalsIgnoreCase("true")) {

                                JSONObject result = object.getJSONObject("result");
                                String user_name = result.getString("user_name");
                                String email = result.getString("email");
                                String phone_no = result.getString("phone_no");
                                String locality = result.getString("locality");
                                String address = result.getString("address");
                                String latitude = result.getString("latitude");
                                String longitude = result.getString("longitude");

                                SharedPreferences sp = getApplicationContext().getSharedPreferences("Laundry", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sp.edit();
                                editor.putString("user_name", user_name);
                                editor.putString("email", email);
                                editor.putString("phone_no", phone_no);
                                editor.putString("address", address);
                                editor.putString("locality", locality);
                                editor.putString("latitude", latitude);
                                editor.putString("longitude", longitude);
                                editor.apply();

                                Helper.showToast(EditProfileActivity.this, message);

                                Intent intent = new Intent(EditProfileActivity.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();

                            } else {
                                Helper.showToast(EditProfileActivity.this, message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onAPIError(int requestCode, boolean isError, String error) {
                        if (isError) {
                            Helper.showToast(EditProfileActivity.this, error);
                        }
                    }
                };

                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", user_id);
                params.put("user_name", userName);
                params.put("email", email);
                params.put("phone_no", phoneNo);
                params.put("password", password);
                params.put("locality", Locality);
                params.put("address", user_Address);
                params.put("latitude", Latitude);
                params.put("longitude", Longitude);

                API api = new API(EditProfileActivity.this, apiResponse);
                api.execute(1, Services.EDIT_PROFILE, params, true);
            } else {
                Helper.showToast(EditProfileActivity.this, Services.NO_NETWORK);
            }
        }
    }

    public void onTermsConditions(View view){

        Intent intent = new Intent(EditProfileActivity.this, TermsConditionActivity.class);
        startActivity(intent);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            mMap = googleMap;

            SharedPreferences sp = getApplicationContext().getSharedPreferences("Laundry", Context.MODE_PRIVATE);
            String user_name = sp.getString("user_name", "abc").trim();
            String address = sp.getString("address", "US").trim();
            String Latitude = sp.getString("latitude", "0.0").trim();
            String Longitude = sp.getString("longitude", "0.0").trim();

            if(address.isEmpty() || address.equals("null")){
                address = "US";
            }

            if(Latitude.isEmpty() || Latitude.equals("null") || Longitude.isEmpty() || Longitude.equals("null")){
                Latitude = "0.0";
                Longitude = "0.0";
            }

            latitude = Double.parseDouble(Latitude);
            longitude = Double.parseDouble(Longitude);

            if (!TextUtils.isEmpty(address) && address.length() > 0) {
                LatLng latLng = new LatLng(Double.parseDouble(Latitude), Double.parseDouble(Longitude));
                Log.e("LATLONG",""+latLng);
                if (mMap != null) {
                    map_marker = mMap.addMarker(new MarkerOptions().position(latLng).title(user_name).draggable(true));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);

                    Log.e("MARKER ADDED","TRUE");
                    //////////// My Coding start /////////////////
                    mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                        @Override
                        public void onMarkerDragStart(Marker marker) {

                        }

                        @Override
                        public void onMarkerDrag(Marker marker) {

                        }

                        @Override
                        public void onMarkerDragEnd(Marker marker) {

                            LatLng position = marker.getPosition(); //
                            latitude =  position.latitude;
                            longitude = position.longitude;
                            Log.e("Laundry After DRAG","Lat " + position.latitude + " "
                                    + "Long " + position.longitude );
                        }
                    });
                    //////////// My Coding end /////////////////

                } else {
                    Helper.showLog("Map is null");
                }

            } else {
                Helper.showLog("User location is null");
            }
        }

        catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   /* @Override
    protected void onStart() {
        super.onStart();
        if (this.mGoogleApiClient != null) {
            this.mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }*/

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("Tag Fail", connectionResult.getErrorMessage());
    }

}
