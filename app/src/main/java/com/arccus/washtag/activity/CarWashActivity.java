package com.arccus.washtag.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.arccus.washtag.R;
import com.arccus.washtag.model.CarPackageModel;
import com.arccus.washtag.model.CarType;
import com.arccus.washtag.utils.API;
import com.arccus.washtag.utils.APIResponse;
import com.arccus.washtag.utils.Helper;
import com.arccus.washtag.utils.Services;
import com.arccus.washtag.utils.Validate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CarWashActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView ivBack;
    Spinner sp_car_type;
    EditText ed_name, ed_contact_no, ed_car_no, ed_pick_address, ed_drop_address;
    Button btn_submit;
    GridLayout rg_grid;
    //    RadioButton rb_ind, rb_yearly;
    TextView txt_msg;
    ArrayList<String> cT;

    String car_type = "", car_package_id = "";
    List<CarPackageModel> packageModels = new ArrayList<>();
    RadioGroup.OnCheckedChangeListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_wash);
        initView();
    }

    private void initView() {

        ivBack = findViewById(R.id.ivBack);
        sp_car_type = findViewById(R.id.sp_car_type);
        //        sp_car_package = findViewById(R.id.sp_car_package);
        ed_name = findViewById(R.id.ed_name);
        ed_contact_no = findViewById(R.id.ed_contact_no);
        ed_car_no = findViewById(R.id.ed_car_no);
        ed_pick_address = findViewById(R.id.ed_pick_address);
        ed_drop_address = findViewById(R.id.ed_drop_address);
        btn_submit = findViewById(R.id.btn_submit);
        rg_grid = findViewById(R.id.rg_grid);
        //        rb_ind = findViewById(R.id.rb_ind);
        //        rb_yearly = findViewById(R.id.rb_yearly);
        txt_msg = findViewById(R.id.txt_msg);

        ivBack.setOnClickListener(this);

        /*cT = new ArrayList<>();
        cT.add("Small");
        cT.add("Hatchback");
        cT.add("Sedan");
        cT.add("SUV");

        CarPackageModel model = new CarPackageModel();
        model.setPackage_id("1");
        model.setCarType_name("Small");
        model.setTime_period("Individual");
        model.setDescription("");
        model.setPrice("250");
        model.setDate("22");
        packageModels.add(model);
        CarPackageModel model1 = new CarPackageModel();
        model1.setPackage_id("2");
        model1.setCarType_name("Small");
        model1.setTime_period("Yearly");
        model1.setDescription("2 wash extra");
        model1.setPrice("3000");
        model1.setDate("24");
        packageModels.add(model1);
        CarPackageModel model2 = new CarPackageModel();
        model2.setPackage_id("3");
        model2.setCarType_name("Hatchback");
        model2.setTime_period("Individual");
        model2.setDescription("");
        model2.setPrice("500");
        model2.setDate("26");
        packageModels.add(model2);
        CarPackageModel model3 = new CarPackageModel();
        model3.setPackage_id("4");
        model3.setCarType_name("Hatchback");
        model3.setTime_period("Yearly");
        model3.setDescription("2 wash extra");
        model3.setPrice("6000");
        model3.setDate("30");
        packageModels.add(model3);
        CarPackageModel model4 = new CarPackageModel();
        model4.setPackage_id("5");
        model4.setCarType_name("Sedan");
        model4.setTime_period("Individual");
        model4.setDescription("");
        model4.setPrice("300");
        model4.setDate("26");
        packageModels.add(model4);
        CarPackageModel model5 = new CarPackageModel();
        model5.setPackage_id("6");
        model5.setCarType_name("Sedan");
        model5.setTime_period("Yearly");
        model5.setDescription("2 wash extra");
        model5.setPrice("3600");
        model5.setDate("30");
        packageModels.add(model5);
        CarPackageModel model6 = new CarPackageModel();
        model6.setPackage_id("7");
        model6.setCarType_name("Sedan");
        model6.setTime_period("Individual");
        model6.setDescription("");
        model6.setPrice("400");
        model6.setDate("26");
        packageModels.add(model6);
        CarPackageModel model7 = new CarPackageModel();
        model7.setPackage_id("8");
        model7.setCarType_name("Sedan");
        model7.setTime_period("Yearly");
        model7.setDescription("2 wash extra");
        model7.setPrice("4800");
        model7.setDate("30");
        packageModels.add(model7);*/

        getCarPackage();
//        rg_group.setOnCheckedChangeListener(this);
        btn_submit.setOnClickListener(this);

    }

    public void AddSpinner(List<String> carType) {

        /*ArrayList<String> carType = new ArrayList<>();
        carType.add("Small");
        carType.add("Hatchback");
        carType.add("Sedan");
        carType.add("SUV");*/

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, carType);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_car_type.setAdapter(adapter);

        sp_car_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                car_type = adapterView.getItemAtPosition(i).toString();

                final List<CarPackageModel> packages = new ArrayList<>();
                for (int c = 0; c < packageModels.size(); c++) {
                    if (packageModels.get(c).getCarType_name().equals(car_type)) {
                        packages.add(packageModels.get(c));
                        rg_grid.removeAllViews();
                    }
                }

                if (packages != null && packages.size() > 0) {

                    rg_grid.removeAllViews();

                    int row = 1;
                    if (packages != null && packages.size() > 2 && (packages.size() / 2) > 0) {
                        row = Math.round(packages.size() / 2);
                        Log.e("Tag Result", "" + row);
                        Log.e("Tag Result", "" + packages.size() / 2);
                    }
                    Log.e("Tag Result", "" + packages.size());
                    rg_grid.setRowCount(row);
                    rg_grid.setColumnCount(2);

                    for (int r = 0; r < packages.size(); r++) {

                        RadioButton button = new RadioButton(CarWashActivity.this);
                        button.setText(wordFirstCap(packages.get(r).getTime_period()));
                        button.setTextColor(Color.BLACK);
                        button.setGravity(Gravity.CENTER);
                        button.setTypeface(getResources().getFont(R.font.my_custom_font));
                        button.setElevation(6f);
                        button.setPadding(5, 5, 5, 5);
                        button.setButtonDrawable(null);
                        button.setId(r);
                        button.setBackground(getDrawable(R.drawable.radio_btn));
                        button.setTextSize(getResources().getDimension(R.dimen._8sdp));
                        final int finalR = r;
                        button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                for (int r = 0; r < rg_grid.getChildCount(); r++) {
                                    RadioButton rb = (RadioButton) rg_grid.getChildAt(r);
                                    if (compoundButton.getId() != rb.getId()) {
                                        rb.setOnCheckedChangeListener(null);
                                        rb.setChecked(false);
                                        rb.setOnCheckedChangeListener(this);
                                        rb.setTextColor(Color.BLACK);
                                    } else {
                                        rb.setOnCheckedChangeListener(null);
                                        rb.setChecked(true);
                                        rb.setOnCheckedChangeListener(this);
                                        rb.setTextColor(Color.WHITE);
                                        String dash = packages.get(r).getDescription().equals("") ? "  " : " (<i>" + packages.get(r).getDescription() + "</i>)";
                                        txt_msg.setText(Html.fromHtml("<b>Price : \u20B9 " + packages.get(r).getPrice()+ ".00 " + dash+"</b>"));
                                        car_package_id = packages.get(r).getPackage_id();
                                        Log.e("Tag Package Id", car_package_id);
                                    }
                                }
                            }
                        });

                        GridLayout.LayoutParams par = new GridLayout.LayoutParams(GridLayout.spec(GridLayout.UNDEFINED, 1f),
                                GridLayout.spec(GridLayout.UNDEFINED, 1f));
                        par.width = 0;
                        par.leftMargin = 20;
                        par.rightMargin = 20;
                        par.topMargin = 10;
                        par.bottomMargin = 10;
                        rg_grid.addView(button, par);

                        ((RadioButton) rg_grid.getChildAt(0)).setChecked(true);
                    }
                }

                /*if (rg_group.getCheckedRadioButtonId() == R.id.rb_ind) {
                    rg_group.check(R.id.rb_Yearly);
                } else {
                    rg_group.check(R.id.rb_ind);
                }*/

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        /*ArrayAdapter<String> PackageAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, CarPackage);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_car_package.setAdapter(PackageAdapter);

        sp_car_package.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    car_package_id = "";
                } else {
                    car_package_id = adapterView.getItemAtPosition(i).toString();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                car_package_id = "";
            }
        });*/
    }

    public void validation() {

        if (ed_name.getText().toString().equals("")) {
            ed_name.requestFocus();
            ed_name.setError("Empty Field");
        } else if (ed_contact_no.getText().toString().equals("")) {
            ed_contact_no.requestFocus();
            ed_contact_no.setError("Empty Field");
        } else if (!Validate.isValidMobile(ed_contact_no.getText().toString())) {
            ed_contact_no.requestFocus();
            ed_contact_no.setError("Contact InValid");
        } else if (ed_car_no.getText().toString().equals("")) {
            ed_car_no.requestFocus();
            ed_car_no.setError("Empty Field");
        } else if (ed_pick_address.getText().toString().equals("")) {
            ed_pick_address.requestFocus();
            ed_pick_address.setError("Empty Field");
        } else if (ed_drop_address.getText().toString().equals("")) {
            ed_drop_address.requestFocus();
            ed_drop_address.setError("Empty Field");
        } else {
//            Toast.makeText(getApplicationContext(), "coming soon Car Service", Toast.LENGTH_LONG).show();
            AddService();
        }
    }

    @Override
    public void onClick(View view) {
        if (ivBack == view) {
            onBackPressed();
        } else if (view == btn_submit) {
            validation();
        }
    }

    private void AddService() {

        APIResponse response = new APIResponse() {
            @Override
            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                if (isSuccess) {
                    Log.e("Response", response);
                    try {
                        JSONObject object = new JSONObject(response);
                        Helper.showToast(getApplicationContext(), object.getString("message"));
                        onBackPressed();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("Tag Exception", e.toString());
                    }
                }
            }

            @Override
            public void onAPIError(int requestCode, boolean isError, String error) {
                Log.e("Response", error);
                onBackPressed();
//                Helper.showToast(getApplicationContext(), error);
            }
        };

        API api = new API(CarWashActivity.this, response);
        HashMap<String, String> map = new HashMap<>();
//        map.put("service", "car");
        map.put("name", ed_name.getText().toString());
        map.put("contact", ed_contact_no.getText().toString());
        map.put("package_id", car_package_id);
        map.put("car_type", car_type);
        map.put("car_no", ed_car_no.getText().toString());
        map.put("pic_address", ed_pick_address.getText().toString());
        map.put("drop_address", ed_drop_address.getText().toString());

        api.execute(1, Services.BASE_URL + "add_car_wash", map, true);
    }

    /*@Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {

        listener = this;
        if (radioGroup.getCheckedRadioButtonId() == R.id.rb_ind) {
            if (car_type.equals("Small")) {
                txt_msg.setText("Rs. 250");
            } else if (car_type.equals("Hatchback")) {
                txt_msg.setText("Rs. 300");
            } else if (car_type.equals("Sedan")) {
                txt_msg.setText("Rs. 400");
            } else if (car_type.equals("SUV")) {
                txt_msg.setText("Rs. 500");
            }
        } else {
            if (car_type.equals("Small")) {
                txt_msg.setText("Rs. 3000 - extra 2 wash");
            } else if (car_type.equals("Hatchback")) {
                txt_msg.setText("Rs. 3600 - extra 2 wash");
            } else if (car_type.equals("Sedan")) {
                txt_msg.setText("Rs. 4800 - extra 2 wash");
            } else if (car_type.equals("SUV")) {
                txt_msg.setText("Rs. 6000 - extra 2 wash");
            }
        }
    }*/

    private void getCarPackage() {

        APIResponse response = new APIResponse() {
            @Override
            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                if (isSuccess) {
                    Log.e("Response", response);
                    try {
                       /* JSONObject object = new JSONObject(response);
                        if (object.getString("status").equals("true")) {*/
                            List<String> carType = new ArrayList<>();
                            packageModels = new ArrayList<>();
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                String type = jsonObject.getString("type");
                                if (!carType.contains(wordFirstCap(type))) {
                                    carType.add(wordFirstCap(type));
                                }
                                CarPackageModel model = new CarPackageModel();
                                model.setPackage_id(jsonObject.getString("id"));
                                model.setCarType_name(wordFirstCap(type));
//                                model.setDate(jsonObject.getString("create_at"));
                                model.setDescription(jsonObject.getString("description"));
                                model.setPrice(jsonObject.getString("price"));
                                model.setTime_period(jsonObject.getString("time_period"));
                                packageModels.add(model);
                            }
                            AddSpinner(carType);
                       /* } else {
                            Helper.showToast(getApplicationContext(), object.getString("message"));
                        }*/
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("Tag Exception", e.toString());
                        Helper.showToast(getApplicationContext(), "Something went wrong");
                    }
                }
            }

            @Override
            public void onAPIError(int requestCode, boolean isError, String error) {
                Log.e("Response", error);
                Helper.showToast(getApplicationContext(), error);
//                AddSpinner(cT);
            }
        };

        API api = new API(CarWashActivity.this, response);
        HashMap<String, String> map = new HashMap<>();

        api.execute(1, Services.BASE_URL + "getCarPackage", map, true);

    }

    public String wordFirstCap(String s) {
        if (s.equals("")){
            return "";
        }
        final String DELIMITERS = " '-/";

        StringBuilder sb = new StringBuilder();
        boolean capNext = true;
        for (char c : s.toCharArray()) {
            Log.e("Tag String ", ""+c);
            c = (capNext)
                    ? Character.toUpperCase(c)
                    : Character.toLowerCase(c);
            sb.append(c);
            capNext = (DELIMITERS.indexOf((int) c) >= 0);
            Log.e("Tag String ", ""+capNext);
        }
        return sb.toString();
    }
}
