package com.arccus.washtag.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.TextView;

import com.arccus.washtag.R;
import com.arccus.washtag.adapter.OrderHistoryAdapter;
import com.arccus.washtag.model.OrderHistoryData;
import com.arccus.washtag.utils.API;
import com.arccus.washtag.utils.APIResponse;
import com.arccus.washtag.utils.Helper;
import com.arccus.washtag.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MyOrderActivity extends AppCompatActivity {

    private ImageView ivBack;

    private RecyclerView rvOrderHistory;
    private LinearLayoutManager orderHistoryLayoutManager;
    private OrderHistoryAdapter orderHistoryAdapter;
    private OrderHistoryData orderHistoryData;
    private ArrayList<OrderHistoryData> orderHistoryArrayList;

    private TextView tv_customer_name, tv_package_name, tv_time_limit, tv_start_date,
            tv_end_date, tv_cloth_limit, tv_current_cloth_limit;

    private  String user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_order);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ivBack = (ImageView) findViewById(R.id.ivBack);

        tv_customer_name = (TextView) findViewById(R.id.tv_customer_name);
        tv_package_name = (TextView) findViewById(R.id.tv_package_name);
        tv_time_limit = (TextView) findViewById(R.id.tv_time_limit);
        tv_start_date = (TextView) findViewById(R.id.tv_start_date);
        tv_end_date = (TextView) findViewById(R.id.tv_end_date);
        tv_cloth_limit = (TextView) findViewById(R.id.tv_cloth_limit);
        tv_current_cloth_limit = (TextView) findViewById(R.id.tv_current_cloth_limit);

        SharedPreferences sp = getApplicationContext().getSharedPreferences("Laundry", Context.MODE_PRIVATE);
        user_id = sp.getString("user_id", "").trim();
        String user_name = sp.getString("user_name", "").trim();
        tv_customer_name.setText(user_name);

        rvOrderHistory = (RecyclerView) findViewById(R.id.rvOrderHistory);
        orderHistoryLayoutManager = new LinearLayoutManager(MyOrderActivity.this);
        orderHistoryLayoutManager.scrollToPosition(0);
        // overlayLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvOrderHistory.setLayoutManager(orderHistoryLayoutManager);
        rvOrderHistory.setHasFixedSize(true);

        getPackageInfo();

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getPackageInfo(){
        if (Helper.isNetworkAvailable(MyOrderActivity.this)) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Helper.showLog("PACKAGE INFO. : " + response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            JSONArray data = object.getJSONArray("result");

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject result = data.getJSONObject(i);
                                String package_name = result.getString("package_name");
                                String package_plan_period = result.getString("package_plan_period");
                                String booked_package_date = result.getString("booked_package_date");
                                String booked_package_expire_date = result.getString("booked_package_expire_date");
                                String booked_cloth_limit = result.getString("booked_cloth_limit");
                                String remain_total_cloths = result.getString("remain_total_cloths");

                                tv_package_name.setText(package_name);
                                tv_time_limit.setText(package_plan_period+ " Month");
                                tv_start_date.setText(booked_package_date);
                                tv_end_date.setText(booked_package_expire_date);
                                tv_cloth_limit.setText(booked_cloth_limit + " Clothes Monthly");
                                tv_current_cloth_limit.setText(remain_total_cloths + " Clothes");
                            }

                            getOrderHistory();

                        } else {
                            Helper.showToast(MyOrderActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(MyOrderActivity.this, error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", user_id);

            API api = new API(MyOrderActivity.this, apiResponse);
            api.execute(1, Services.GET_PACKAGE_INFO, params, true);

        } else {
            Helper.showToast(MyOrderActivity.this, Services.NO_NETWORK);
        }
    }


    private void getOrderHistory(){

        if (Helper.isNetworkAvailable(MyOrderActivity.this)) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Helper.showLog("ORDER LIST : " + response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            JSONArray data = object.getJSONArray("result");

                            orderHistoryArrayList = new ArrayList<>();

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject result = data.getJSONObject(i);
                                String order_no = result.getString("order_no");
                                String order_date = result.getString("order_date");


                                orderHistoryData = new OrderHistoryData();
                                orderHistoryData.setOrder_no(order_no);
                                orderHistoryData.setOrder_date(order_date);

                                orderHistoryArrayList.add(orderHistoryData);
                            }

                            orderHistoryAdapter = new OrderHistoryAdapter( MyOrderActivity.this, orderHistoryArrayList);

                            rvOrderHistory.setAdapter(orderHistoryAdapter);

                            int resId = R.anim.layout_animation_from_bottom;
                            LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(MyOrderActivity.this, resId);
                            rvOrderHistory.setLayoutAnimation(animation);

                        } else {
                            Helper.showToast(MyOrderActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(MyOrderActivity.this, error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", user_id);

            API api = new API(MyOrderActivity.this, apiResponse);
            api.execute(1, Services.GET_ORDER, params, true);

        } else {
            Helper.showToast(MyOrderActivity.this, Services.NO_NETWORK);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
