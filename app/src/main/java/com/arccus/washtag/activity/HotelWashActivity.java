package com.arccus.washtag.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.arccus.washtag.R;
import com.arccus.washtag.utils.API;
import com.arccus.washtag.utils.APIResponse;
import com.arccus.washtag.utils.Helper;
import com.arccus.washtag.utils.Services;
import com.arccus.washtag.utils.Validate;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class HotelWashActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView ivBack;
    EditText ed_name, ed_contact_no, ed_hotel_name, ed_rooms, ed_address;
    Button btn_submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_wash);
        initView();
    }

    private void initView() {

        ivBack = findViewById(R.id.ivBack);
        ed_name = findViewById(R.id.ed_name);
        ed_contact_no = findViewById(R.id.ed_contact_no);
        ed_hotel_name = findViewById(R.id.ed_hotel_name);
        ed_rooms = findViewById(R.id.ed_room);
        ed_address = findViewById(R.id.ed_address);
        btn_submit = findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(this);
        ivBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (ivBack == view) {
            onBackPressed();
        } else if (btn_submit == view) {
            validation();
        }
    }

    public void validation() {

        if (ed_name.getText().toString().equals("")) {
            ed_name.requestFocus();
            ed_name.setError("Enter your Name");
        } else if (ed_contact_no.getText().toString().equals("")) {
            ed_contact_no.requestFocus();
            ed_contact_no.setError("Enter contact number");
        } else if (!Validate.isValidMobile(ed_contact_no.getText().toString())) {
            ed_contact_no.requestFocus();
            ed_contact_no.setError("Contact InValid");
        } else if (ed_hotel_name.getText().toString().equals("")) {
            ed_hotel_name.requestFocus();
            ed_hotel_name.setError("Enter Hotel name");
        } else if (ed_rooms.getText().toString().equals("")) {
            ed_rooms.requestFocus();
            ed_rooms.setError("Enter Room");
        } else if (ed_address.getText().toString().equals("")) {
            ed_address.requestFocus();
            ed_address.setError("Enter Address");
        } else {
//            Toast.makeText(getApplicationContext(),"coming soon Hotel Service", Toast.LENGTH_LONG).show();
            AddService();
        }
    }

    private void AddService() {

        APIResponse response = new APIResponse() {
            @Override
            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                if (isSuccess) {
                    Log.e("Response", response);
                    try {
                        JSONObject object = new JSONObject(response);
                        Helper.showToast(getApplicationContext(), object.getString("message"));
                        onBackPressed();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("Tag Exception", e.toString());
                    }
                }
            }

            @Override
            public void onAPIError(int requestCode, boolean isError, String error) {
                Log.e("Response", error);
                onBackPressed();
//                Helper.showToast(getApplicationContext(), error);
            }
        };

        API api = new API(HotelWashActivity.this, response);
        HashMap<String, String> map = new HashMap<>();
        map.put("service", "hotel");
        map.put("name", ed_name.getText().toString());
        map.put("contact", ed_contact_no.getText().toString());
        map.put("hotel_name", ed_hotel_name.getText().toString());
        map.put("no_rooms", ed_rooms.getText().toString());
        map.put("address", ed_address.getText().toString());

        api.execute(1, Services.BASE_URL + "add_hotel_wash", map, true);
    }

}
