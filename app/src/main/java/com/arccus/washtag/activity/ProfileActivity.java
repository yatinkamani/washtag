package com.arccus.washtag.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.arccus.washtag.R;

public class ProfileActivity extends AppCompatActivity {

    private ImageView ivBack;
    private TextView tvUserName, tvEmialAddress, tvPhoneNo, tvUserAddress;
    Animation animMove;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        tvUserName = (TextView) findViewById(R.id.tvUserName);
        tvEmialAddress = (TextView) findViewById(R.id.tvEmialAddress);
        tvPhoneNo = (TextView) findViewById(R.id.tvPhoneNo);
        tvUserAddress = (TextView) findViewById(R.id.tvUserAddress);

        ivBack = (ImageView) findViewById(R.id.ivBack);

        animMove = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.move);

        SharedPreferences sp = getApplicationContext().getSharedPreferences("Laundry", Context.MODE_PRIVATE);
        String user_name = sp.getString("user_name", "").trim();
        String address = sp.getString("address", "").trim();
        String phone_no = sp.getString("phone_no", "").trim();
        String email = sp.getString("email", "").trim();

        tvUserName.setText(user_name);
        tvEmialAddress.setText(email);
        tvPhoneNo.setText(phone_no);
        if(address.equals("null") || address.isEmpty()){
            tvUserAddress.setText("");
        }else {
            tvUserAddress.setText(address);
        }

        tvUserName.startAnimation(animMove);
        tvEmialAddress.startAnimation(animMove);
        tvPhoneNo.startAnimation(animMove);
        tvUserAddress.startAnimation(animMove);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
