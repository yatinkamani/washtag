package com.arccus.washtag.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.arccus.washtag.R;
import com.arccus.washtag.utils.APIResponse;

public class TermsConditionActivity extends AppCompatActivity {

    private ImageView ivBack;
    private TextView tv_terms_conditions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_condition);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ivBack = (ImageView) findViewById(R.id.ivBack);
        tv_terms_conditions = (TextView) findViewById(R.id.tv_terms_conditions);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    private void getTermConditionData(){

        APIResponse response = new APIResponse() {
            @Override
            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {

            }

            @Override
            public void onAPIError(int requestCode, boolean isError, String error) {

            }
        };

    }
}
