package com.arccus.washtag.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arccus.washtag.R;
import com.arccus.washtag.utils.API;
import com.arccus.washtag.utils.APIResponse;
import com.arccus.washtag.utils.Helper;
import com.arccus.washtag.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;

public class PackageDetailActivity extends AppCompatActivity {

    private ImageView ivBack;
    private String package_id, user_id;
    private LinearLayout llPurchase;
    private TextView tv_package_name, tv_package_period, tv_package_person, tv_package_cloth_limit,
            tv_package_price, tv_discount, tv_discount_price, tv_final_price, tvPurchase;
    Animation animMove;
    private RelativeLayout rootView;
    DecimalFormat df;
    private String package_status = "";
    private String show_message = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        package_id = getIntent().getStringExtra("PackagePlanId");

        SharedPreferences sp = getApplicationContext().getSharedPreferences("Laundry", Context.MODE_PRIVATE);
        user_id = sp.getString("user_id", "").trim();

        df = new DecimalFormat("0.00");

        initViews();

    }

    private void initViews() {

        tv_package_name = (TextView) findViewById(R.id.tv_package_name);
        tv_package_period = (TextView) findViewById(R.id.tv_package_period);
        // tv_package_person = (TextView) findViewById(R.id.tv_package_persion);
        tv_package_cloth_limit = (TextView) findViewById(R.id.tv_package_cloth_limit);
        tv_package_price = (TextView) findViewById(R.id.tv_package_price);
        tv_discount = (TextView) findViewById(R.id.tv_discount);
        tv_discount_price = (TextView) findViewById(R.id.tv_discount_price);
        tv_final_price = (TextView) findViewById(R.id.tv_final_price);
        tvPurchase = (TextView) findViewById(R.id.tvPurchase);

        llPurchase = (LinearLayout) findViewById(R.id.llPurchase);
        rootView = (RelativeLayout) findViewById(R.id.rootView);
        ivBack = (ImageView) findViewById(R.id.ivBack);

        getPackageDetails();

        animMove = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.move);

        tv_package_name.startAnimation(animMove);
        tv_package_period.startAnimation(animMove);
        //tv_package_person.startAnimation(animMove);
        tv_package_cloth_limit.startAnimation(animMove);
        tv_package_price.startAnimation(animMove);
        tv_discount.startAnimation(animMove);
        tv_discount_price.startAnimation(animMove);
        tv_final_price.startAnimation(animMove);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        llPurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (package_status.equalsIgnoreCase("0")) {
                    openDialog();
                } else if (package_status.equalsIgnoreCase("1") || package_status.equalsIgnoreCase("2")) {
                    openRemainClothDialog();
                }
            }
        });
    }

    private void getPackageDetails() {
        if (Helper.isNetworkAvailable(PackageDetailActivity.this)) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Helper.showLog("Package List : " + response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            llPurchase.setVisibility(View.VISIBLE);

                            JSONArray data = object.getJSONArray("result");

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject result = data.getJSONObject(i);
                                String package_plan_period = result.getString("package_plan_period");
                                String package_plan_person = result.getString("package_plan_person");
                                String package_plan_cloth_limit = result.getString("package_plan_cloth_limit");
                                String package_plan_price = result.getString("package_plan_price");
                                String package_plan_discount = result.getString("package_plan_discount");
                                String package_plan_discount_price = result.getString("package_plan_discount_price");
                                String package_plan_final_price = result.getString("package_plan_final_price");
                                String package_name = result.getString("package_name");
                                package_status = result.getString("status");

                                tv_package_name.setText(package_name);
                                tv_package_period.setText(package_plan_period);
                                //  tv_package_person.setText(package_plan_person);
                                tv_package_cloth_limit.setText(package_plan_cloth_limit);
                                tv_package_price.setText(getString(R.string.Rs) + df.format(Double.parseDouble(package_plan_price)));
                                if (TextUtils.isEmpty(package_plan_discount)) {
                                    tv_discount.setText("0%");
                                } else {
                                    tv_discount.setText(package_plan_discount + "%");
                                }
                                if (TextUtils.isEmpty(package_plan_discount_price)) {
                                    tv_discount_price.setText("0.00");
                                } else {
                                    tv_discount_price.setText(getString(R.string.Rs) + df.format(Double.parseDouble(package_plan_discount_price)));
                                }
                                tv_final_price.setText(getString(R.string.Rs) + df.format(Double.parseDouble(package_plan_final_price)));

                                if (package_status.equalsIgnoreCase("0")) {
                                    tvPurchase.setText("Purchase Now");
                                } else if (package_status.equalsIgnoreCase("1")) {
                                    tvPurchase.setText("Renew");
                                    String remain_days = result.getString("remain_days");
                                    show_message = "Your package is still active and you have " + remain_days +
                                            " days remaining to expire. Do you want to renew?";
                                } else if (package_status.equalsIgnoreCase("2")) {
                                    tvPurchase.setText("Upgrade");
                                    String remain_days = result.getString("remain_days");
//                                    show_message = result.getString("remain_days");
                                    show_message = "Your package is still active and you have " + remain_days +
                                            " days remaining to expire. Do you want to upgrade?";
                                }
                            }

                        } else {
                            Helper.showToast(PackageDetailActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(PackageDetailActivity.this, error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("package_plan_id", package_id);
            params.put("user_id", user_id);

            API api = new API(PackageDetailActivity.this, apiResponse);
            api.execute(1, Services.PACKAGE_DETAIL, params, true);
        } else {
            Helper.showToast(PackageDetailActivity.this, Services.NO_NETWORK);
        }
    }

    private void openRemainClothDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(PackageDetailActivity.this);
        alertDialogBuilder.setMessage(show_message);
        alertDialogBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        openDialog();
                    }
                });

        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
    }

    public void openDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(PackageDetailActivity.this);
//        LayoutInflater inflater = LoginActivity.this.getLayoutInflater();
        View dialogView = LayoutInflater.from(PackageDetailActivity.this).inflate(R.layout.payment_popup_dialog, rootView, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        TextView dialog_confirm = (TextView) dialogView.findViewById(R.id.dialog_confirm);
        TextView dialog_cancel = (TextView) dialogView.findViewById(R.id.dialog_cancel);

        dialog_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Helper.isNetworkAvailable(PackageDetailActivity.this)) {
                    APIResponse apiResponse = new APIResponse() {
                        @Override
                        public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                            Helper.showLog("BUY PACKAGE : " + response);
                            try {
                                JSONObject object = new JSONObject(response);
                                String status = object.getString("status");
                                String message = object.getString("message");
                                if (status.equalsIgnoreCase("true")) {
                                    if (alertDialog.isShowing()) {
                                        alertDialog.dismiss();
                                    }
                                    Toast.makeText(PackageDetailActivity.this, message, Toast.LENGTH_LONG).show();
                                    // Helper.showToast(PackageDetailActivity.this, message);

                                } else {
                                    Helper.showToast(PackageDetailActivity.this, message);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Helper.showToast(PackageDetailActivity.this, "something wrong");
                            }
                        }

                        @Override
                        public void onAPIError(int requestCode, boolean isError, String error) {
                            if (isError) {
                                Helper.showToast(PackageDetailActivity.this, error);
                                if (alertDialog.isShowing()) {
                                    alertDialog.dismiss();
                                }
                            }
                        }
                    };

                    HashMap<String, String> params = new HashMap<>();
                    params.put("user_id", user_id);
                    params.put("package_id", package_id);

                    API api = new API(PackageDetailActivity.this, apiResponse);
                    api.execute(1, Services.BUY_PACKAGE, params, true);

                } else {
                    Helper.showToast(PackageDetailActivity.this, Services.NO_NETWORK);
                }
            }
        });

        dialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }
}
