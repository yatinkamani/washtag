package com.arccus.washtag.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.arccus.washtag.R;

import java.util.Calendar;

public class ScheduleActivity extends AppCompatActivity implements
        View.OnClickListener{

    private ImageView ivBack, ivCalender, ivClock;
   // Button btnPicDate, btnPicTime, btnDeliveryDate, btnDeliveryTime;
    TextView txtPicDate, txtPicTime, txtDeliveryDate, txtDeliveryTime;
    private int pYear, pMonth, pDay, pHour, pMinute, dYear, dMonth, dDay, dHour, dMinute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ivBack = (ImageView) findViewById(R.id.ivBack);
        ivCalender = (ImageView) findViewById(R.id.ivCalender);
        ivClock = (ImageView) findViewById(R.id.ivClock);

       /* btnPicDate=(Button)findViewById(R.id.btn_pickdate);
        btnPicTime=(Button)findViewById(R.id.btn_picktime);
        btnDeliveryDate=(Button)findViewById(R.id.btn_delivery_date);
        btnDeliveryTime=(Button)findViewById(R.id.btn_delivery_time);*/


        txtPicDate=(TextView)findViewById(R.id.in_pickdate);
        txtPicTime=(TextView)findViewById(R.id.in_picktime);
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/Sofia-Regular.otf");

        txtPicDate.setTypeface(custom_font);
        txtPicTime.setTypeface(custom_font);
        /*txtDeliveryDate=(EditText)findViewById(R.id.in_delivery_date);
        txtDeliveryTime=(EditText)findViewById(R.id.in_deliverytime);*/


        txtPicDate.setOnClickListener(this);
        txtPicTime.setOnClickListener(this);

       /*
       btnPicDate.setOnClickListener(this);
        btnPicTime.setOnClickListener(this);
       btnDeliveryDate.setOnClickListener(this);
        btnDeliveryTime.setOnClickListener(this);*/
        ivBack.setOnClickListener(this);
        ivCalender.setOnClickListener(this);
        ivClock.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == ivCalender) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            pYear = c.get(Calendar.YEAR);
            pMonth = c.get(Calendar.MONTH);
            pDay = c.get(Calendar.DAY_OF_MONTH);


           /* DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            txtPicDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }
                    }, pYear, pMonth, pDay);
            datePickerDialog.getDatePicker().setCalendarViewShown(false);

            datePickerDialog.show();*/

            DatePickerDialog dialog = new DatePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                    txtPicDate.setText(day + "-" + (month + 1) + "-" + year);
                }
            }, pYear, pMonth, pDay);

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            dialog.show();
        }

        /*if (v == btnDeliveryDate) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            dYear = c.get(Calendar.YEAR);
            dMonth = c.get(Calendar.MONTH);
            dDay = c.get(Calendar.DAY_OF_MONTH);


           *//* DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            txtDeliveryDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }
                    }, dYear, dMonth, dDay);
            datePickerDialog.show();*//*

            DatePickerDialog dialog = new DatePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                    txtDeliveryDate.setText(day + "-" + (month + 1) + "-" + year);
                }
            }, dYear, dMonth, dDay);

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            dialog.show();
        }*/

        if (v == ivClock) {

            // Get Current Time
            final Calendar c = Calendar.getInstance();
            pHour = c.get(Calendar.HOUR_OF_DAY);
            pMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            txtPicTime.setText(hourOfDay + ":" + minute);

                        }
                    }, pHour, pMinute, false);
            timePickerDialog.show();

           /* TimePickerDialog timePickerDialog = new TimePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay,
                                      int minute) {

                    txtPicTime.setText(hourOfDay + ":" + minute);
                }
            }, pHour, pMinute, false);

            timePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            timePickerDialog.show();*/

        }

        /*if (v == btnDeliveryTime) {

            // Get Current Time
            final Calendar c = Calendar.getInstance();
            dHour = c.get(Calendar.HOUR_OF_DAY);
            dMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
           *//* TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            txtDeliveryTime.setText(hourOfDay + ":" + minute);
                        }
                    }, dHour, dMinute, false);
            timePickerDialog.show();*//*

            TimePickerDialog timePickerDialog = new TimePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay,
                                      int minute) {

                    txtDeliveryTime.setText(hourOfDay + ":" + minute);
                }
            }, dHour, dMinute, false);

            timePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            timePickerDialog.show();

        }*/

        if (v == ivBack){
            onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
