package com.arccus.washtag.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.arccus.washtag.R;
import com.arccus.washtag.helper.SimsLister;
import com.arccus.washtag.utils.API;
import com.arccus.washtag.utils.APIResponse;
import com.arccus.washtag.utils.Helper;
import com.arccus.washtag.utils.Services;
import com.arccus.washtag.utils.SmsReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class OtpVerificationActivity extends AppCompatActivity {

    private EditText inputOtp;
    private Button btn_verify_otp;

    String from = "registration";
    String OTP = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);

        inputOtp = (EditText) findViewById(R.id.inputOtp);
        btn_verify_otp = (Button) findViewById(R.id.btn_verify_otp);

        from = getIntent().getStringExtra("From");
        OTP = getIntent().getStringExtra("OTP");

        SmsReceiver.bindListener(new SimsLister() {
            @Override
            public void messageReceived(String messageText) {
                Log.e("OTP","TRUE");
                inputOtp.setText(messageText);
            }
        });

        btn_verify_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                verifyOTP();
            }
        });
    }


    public void verifyOTP() {
        String otp = inputOtp.getText().toString().trim();
        if (!otp.isEmpty()) {

            /*if(from.equals("login")) {
                Intent intent = new Intent(OtpVerificationActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            } else {*/

            if (OTP.equalsIgnoreCase(otp)) {
                Register();
            } else {
                Toast.makeText(OtpVerificationActivity.this, "OTP does not match!!!", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(OtpVerificationActivity.this, "Please enter the OTP", Toast.LENGTH_SHORT).show();
        }
    }

    private void Register() {

        SharedPreferences sp = getApplicationContext().getSharedPreferences("Laundry", Context.MODE_PRIVATE);
        String userName = sp.getString("user_name", "").trim();
        String email = sp.getString("email", "").trim();
        String phoneNo = sp.getString("phone_no", "").trim();
        String password = sp.getString("password", "").trim();

        if (Helper.isNetworkAvailable(OtpVerificationActivity.this)) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Helper.showLog("Register : " + response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            JSONObject result = object.getJSONObject("data");
                            String user_id = result.getString("user_id");
                            String user_name = result.getString("user_name");
                            String email = result.getString("email");
                            String mobile = result.getString("phone_no");

                            SharedPreferences sp = getApplicationContext().getSharedPreferences("Laundry", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sp.edit();
                            editor.putString("user_id", user_id);
                            editor.putString("user_name", user_name);
                            editor.putString("email", email);
                            editor.putString("phone_no", mobile);
                            editor.putString("password", "");
                            editor.apply();

                            Helper.showToast(OtpVerificationActivity.this, message);

                            Intent intent = new Intent(OtpVerificationActivity.this, PlaceActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();

                        } else {
                            Helper.showToast(OtpVerificationActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(OtpVerificationActivity.this, error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("user_name", userName);
            params.put("email", email);
            params.put("phone_no", phoneNo);
            params.put("password", password);

            API api = new API(OtpVerificationActivity.this, apiResponse);
            api.execute(1, Services.REGISTER, params, true);
        } else {
            Helper.showToast(OtpVerificationActivity.this, Services.NO_NETWORK);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(OtpVerificationActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
