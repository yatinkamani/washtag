package com.arccus.washtag.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ImageView;

import com.arccus.washtag.R;

public class SplashScreenActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 2000;
    private ImageView ivSplash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        ivSplash = (ImageView) findViewById(R.id.ivSplash);

       /* Glide.with(SplashScreenActivity.this)
                .load(R.drawable.splash_screen)
                .into(ivSplash);*/

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

                SharedPreferences sp = getApplicationContext().getSharedPreferences("Laundry", Context.MODE_PRIVATE);
                String user_id = sp.getString("user_id", "").trim();
                String latitude = sp.getString("latitude", "").trim();

                if(user_id.length()>0) {
                    if(latitude.equalsIgnoreCase("null") || TextUtils.isEmpty(latitude) || latitude.equalsIgnoreCase("0.0")){

                        Intent intent = new Intent(SplashScreenActivity.this, PlaceActivity.class);
                        startActivity(intent);
                        finish();

                    }else {
                        Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                } else {
                    Intent i = new Intent(SplashScreenActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }

            }
        }, SPLASH_TIME_OUT);
    }
}
