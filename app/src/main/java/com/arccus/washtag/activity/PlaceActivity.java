package com.arccus.washtag.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.arccus.washtag.R;
import com.arccus.washtag.adapter.PlacesAutoCompleteAdapter;
import com.arccus.washtag.utils.API;
import com.arccus.washtag.utils.APIResponse;
import com.arccus.washtag.utils.Helper;
import com.arccus.washtag.utils.Services;
import com.arccus.washtag.view.WorkaroundMapFragment;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class PlaceActivity extends AppCompatActivity implements OnMapReadyCallback {

    private ImageView ivBack;
    private LatLngBounds US_BOUNDS = new LatLngBounds(new LatLng(37.1747682, -97.7619313), new LatLng(39.7747695, 101.4038086));
    private AutoCompleteTextView userAddress;
    private GoogleMap mMap;
    private EditText etLocality;
    private PlacesAutoCompleteAdapter mPlacesAdapter;
    private GoogleApiClient mGoogleApiClient;
    double latitude, longitude;
    Marker map_marker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mGoogleApiClient = new GoogleApiClient.Builder(PlaceActivity.this)
                .addApi(Places.GEO_DATA_API)
                .build();

        WorkaroundMapFragment mapFragment = ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapUser));
        mapFragment.getMapAsync(PlaceActivity.this);

        ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapUser))
                .setListener(new WorkaroundMapFragment.OnTouchListener() {
                    @Override
                    public void onTouch() {
                      //  nsvSalonInfo.requestDisallowInterceptTouchEvent(true);
                    }
                });

        userAddress = (AutoCompleteTextView) findViewById(R.id.userAddress);
        etLocality  = (EditText) findViewById(R.id.etLocality);

        mPlacesAdapter = new PlacesAutoCompleteAdapter(PlaceActivity.this, android.R.layout.simple_list_item_1, mGoogleApiClient, US_BOUNDS, null);
        userAddress.setOnItemClickListener(mAutocompleteClickListener);
        userAddress.setAdapter(mPlacesAdapter);

        ivBack = (ImageView) findViewById(R.id.ivBack);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlacesAutoCompleteAdapter.PlaceAutocomplete item = mPlacesAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
//            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
//            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

            com.google.android.libraries.places.api.Places.initialize(getApplicationContext(),Services.api_key);
            PlacesClient placesClient = com.google.android.libraries.places.api.Places.createClient(getApplicationContext());
            List<com.google.android.libraries.places.api.model.Place.Field> placeFields = Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.ID,com.google.android.libraries.places.api.model.Place.Field.NAME, com.google.android.libraries.places.api.model.Place.Field.LAT_LNG, com.google.android.libraries.places.api.model.Place.Field.ADDRESS);
            FetchPlaceRequest request = FetchPlaceRequest.builder(placeId, placeFields).build();
            placesClient.fetchPlace(request).addOnSuccessListener(new OnSuccessListener<FetchPlaceResponse>() {
                @Override
                public void onSuccess(FetchPlaceResponse response) {
                    com.google.android.libraries.places.api.model.Place place = response.getPlace();
                    LatLng latLng = place.getLatLng();
                    Helper.showLog("Lat Lang : " + latLng);
                    latitude = latLng.latitude;
                    longitude = latLng.longitude;
                    Helper.showLog("LATITUDE : " + latitude);
                    Helper.showLog("LONGITUDE : " + longitude);
                    if (mMap != null) {
                        map_marker.setPosition(latLng);
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                        mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    if (exception instanceof ApiException) {
                        Toast.makeText(getApplicationContext(), exception.getMessage() + "", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    };

    /*private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e("PLACE", "Place query did not complete. Error: " +places.getStatus().toString());
                return;
            }

            // Selecting the first object buffer.
            final Place place = places.get(0);
            LatLng latLng = place.getLatLng();
            Helper.showLog("Lat Lang : " + latLng);
            latitude = latLng.latitude;
            longitude = latLng.longitude;
            Helper.showLog("LATITUDE : " + latitude);
            Helper.showLog("LONGITUDE : " + longitude);
            if (mMap != null) {
                map_marker.setPosition(latLng);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
            }
        }
    };*/

    public void onAddLocation(View view){

        SharedPreferences sp = getApplicationContext().getSharedPreferences("Laundry", Context.MODE_PRIVATE);
        String user_id = sp.getString("user_id", "").trim();

        String Locality = etLocality.getText().toString().trim();
        String user_Address = userAddress.getText().toString().trim();

        final String Latitude = String.valueOf(latitude);
        final String Longitude = String.valueOf(longitude);


        if(TextUtils.isEmpty(Locality)){
            Helper.showToast(PlaceActivity.this, "Enter locality");
        } else if(TextUtils.isEmpty(user_Address)){
            Helper.showToast(PlaceActivity.this, "Enter address");
        }else if (Latitude.equalsIgnoreCase("0.0") || Longitude.equalsIgnoreCase("0.0")) {
            Helper.showToast(PlaceActivity.this, "Select Location in MAP");
        } else {

            if (Helper.isNetworkAvailable(PlaceActivity.this)) {
                APIResponse apiResponse = new APIResponse() {
                    @Override
                    public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                        Helper.showLog("Place : " + response);
                        try {
                            JSONObject object = new JSONObject(response);
                            String status = object.getString("status");
                            String message = object.getString("message");
                            if (status.equalsIgnoreCase("true")) {

                                    JSONObject result = object.getJSONObject("data");
                                    String locality = result.getString("locality");
                                    String address = result.getString("address");
                                    String latitude = result.getString("latitude");
                                    String longitude = result.getString("longitude");

                                    SharedPreferences sp = getApplicationContext().getSharedPreferences("Laundry", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sp.edit();
                                    editor.putString("address", address);
                                    editor.putString("latitude", latitude);
                                    editor.putString("longitude", longitude);
                                    editor.putString("locality", locality);
                                    editor.apply();

                                Helper.showToast(PlaceActivity.this, message);

                                Intent intent = new Intent(PlaceActivity.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();

                            } else {
                                Helper.showToast(PlaceActivity.this, message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onAPIError(int requestCode, boolean isError, String error) {
                        if (isError) {
                            Helper.showToast(PlaceActivity.this, error);
                        }
                    }
                };

                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", user_id);
                params.put("address", user_Address);
                params.put("locality", Locality);
                params.put("latitude", Latitude);
                params.put("longitude", Longitude);

                API api = new API(PlaceActivity.this, apiResponse);
                api.execute(1, Services.ADD_LOCATION, params, true);
            } else {
                Helper.showToast(PlaceActivity.this, Services.NO_NETWORK);
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            mMap = googleMap;

            SharedPreferences sp = getApplicationContext().getSharedPreferences("Salon&Savvy", Context.MODE_PRIVATE);
            String first_name = sp.getString("user_name", "").trim();
           /* String salon_address = sp.getString("stylist_address", "US").trim();
            String Latitude = sp.getString("stylist_latitude", "0.0").trim();
            String Longitude = sp.getString("stylist_longitude", "0.0").trim();*/

            String salon_address = "US";
            String Latitude = "0.0";
            String Longitude = "0.0";
          //  String first_name = "abc";

            if(salon_address.isEmpty()){
                salon_address = "US";
            }
            if(Latitude.isEmpty() || Longitude.isEmpty()){
                Latitude = "0.0";
                Longitude = "0.0";
            }

            if (!TextUtils.isEmpty(salon_address) && salon_address.length() > 0) {
                LatLng latLng = new LatLng(Double.parseDouble(Latitude), Double.parseDouble(Longitude));
                if (mMap != null) {
                    map_marker = mMap.addMarker(new MarkerOptions().position(latLng).title(first_name).draggable(true));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);

                    //////////// My Coding start /////////////////
                    mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                        @Override
                        public void onMarkerDragStart(Marker marker) {

                        }

                        @Override
                        public void onMarkerDrag(Marker marker) {

                        }

                        @Override
                        public void onMarkerDragEnd(Marker marker) {

                            LatLng position = marker.getPosition(); //
                            latitude =  position.latitude;
                            longitude = position.longitude;
                            Log.e("Laundry After DRAG","Lat " + position.latitude + " "
                                    + "Long " + position.longitude );
                        }
                    });
                    //////////// My Coding end /////////////////

                } else {
                    Helper.showLog("Map is null");
                }

            } else {
                Helper.showLog("User location is null");
            }
        }

        catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (this.mGoogleApiClient != null) {
            this.mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(PlaceActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}
