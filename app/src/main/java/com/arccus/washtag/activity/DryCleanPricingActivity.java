package com.arccus.washtag.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.ImageView;

import com.arccus.washtag.R;
import com.arccus.washtag.adapter.DryCleanNameAdapter;
import com.arccus.washtag.model.DrycleanList;
import com.arccus.washtag.model.DrycleanName;
import com.arccus.washtag.utils.API;
import com.arccus.washtag.utils.APIResponse;
import com.arccus.washtag.utils.Helper;
import com.arccus.washtag.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class DryCleanPricingActivity extends AppCompatActivity {

    private ImageView ivBack;
    private RecyclerView rvDryClean;
    private LinearLayoutManager dryCleanLayoutManager;
    private DryCleanNameAdapter dryCleanAdapter;
    private DrycleanList drycleanList;
    private DrycleanName drycleanName;

    private ArrayList<DrycleanName> dryCleanNameArrayList;
    private ArrayList<DrycleanList> dryCleanArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dry_clean_pricing);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ivBack = (ImageView) findViewById(R.id.ivBack);

        rvDryClean = (RecyclerView) findViewById(R.id.rvDryClean);
        dryCleanLayoutManager = new LinearLayoutManager(DryCleanPricingActivity.this);
        dryCleanLayoutManager.scrollToPosition(0);
        //  packageLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvDryClean.setLayoutManager(dryCleanLayoutManager);
        rvDryClean.setHasFixedSize(true);

        getDryCleanPrice();

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getDryCleanPrice() {

        dryCleanArrayList = new ArrayList<>();
        dryCleanNameArrayList = new ArrayList<>();

        if (Helper.isNetworkAvailable(DryCleanPricingActivity.this)) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Helper.showLog("DRY CLEAN LIST : " + response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            JSONObject result = object.getJSONObject("result");

                            JSONArray dry_cleaning_array = result.getJSONArray("dry_cleaning");
                            for (int j = 0; j < dry_cleaning_array.length(); j++) {

                                JSONObject dry_clean_object = dry_cleaning_array.getJSONObject(j);
                                String cloth_id = dry_clean_object.getString("dry_id");
                                String garment_category_id = dry_clean_object.getString("garment_category_id");
                                String cloth_name = dry_clean_object.getString("garment_name");
                                String cloth_price = dry_clean_object.getString("dryclean_price");

                                drycleanList = new DrycleanList();
                                drycleanList.setCloth_id(cloth_id);
                                drycleanList.setGarment_category_id(garment_category_id);
                                drycleanList.setCloth_name(cloth_name);
                                drycleanList.setCloth_price(cloth_price);
                                dryCleanArrayList.add(drycleanList);
                            }

                            JSONArray garment_category_array = result.getJSONArray("garment_group");

                            for (int i = 0; i < garment_category_array.length(); i++) {

                                JSONObject dry_clean_object = garment_category_array.getJSONObject(i);
                                String dryClean_category_id = dry_clean_object.getString("garment_category_id");
                                String dryClean_category_name = dry_clean_object.getString("garment_category_name");

                                drycleanName = new DrycleanName();
                                drycleanName.setDryclean_id(dryClean_category_id);
                                drycleanName.setDryclean_name(dryClean_category_name);

                                ArrayList<DrycleanList> dryCleanArrayList1 = new ArrayList<>();

                                for (int k = 0; k < dryCleanArrayList.size(); k++) {
                                    DrycleanList dryCleanList1 = dryCleanArrayList.get(k);

                                    if (dryClean_category_id.equalsIgnoreCase(dryCleanList1.getGarment_category_id())) {
                                        dryCleanArrayList1.add(dryCleanList1);
                                    }
                                }

                                drycleanName.setDrycleanListArray(dryCleanArrayList1);
                                dryCleanNameArrayList.add(drycleanName);
                            }

                            dryCleanAdapter = new DryCleanNameAdapter(DryCleanPricingActivity.this, dryCleanNameArrayList);
                            rvDryClean.setAdapter(dryCleanAdapter);

                        } else {
                            Helper.showToast(DryCleanPricingActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(DryCleanPricingActivity.this, error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("email", "");

            API api = new API(DryCleanPricingActivity.this, apiResponse);
            api.execute(1, Services.DRY_CLEANING, params, true);
        } else {
            Helper.showToast(DryCleanPricingActivity.this, Services.NO_NETWORK);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

}
