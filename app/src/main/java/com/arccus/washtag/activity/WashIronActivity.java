package com.arccus.washtag.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.ImageView;

import com.arccus.washtag.R;
import com.arccus.washtag.adapter.PackageNameAdapter;
import com.arccus.washtag.model.PackageList;
import com.arccus.washtag.model.PackageName;
import com.arccus.washtag.utils.API;
import com.arccus.washtag.utils.APIResponse;
import com.arccus.washtag.utils.Helper;
import com.arccus.washtag.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class WashIronActivity extends AppCompatActivity {

    ImageView ivBack;

    private RecyclerView rvPackage;
    LinearLayoutManager packageLayoutManager;
    private PackageNameAdapter packageAdapter;
    private PackageList packageList;
    private PackageName packageName;

    private ArrayList<PackageName> packageNameArrayList;
    private ArrayList<PackageList> packageArrayList;
    String package_category = "", package_category_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wash_iron);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ivBack = (ImageView) findViewById(R.id.ivBack);

        rvPackage = (RecyclerView) findViewById(R.id.rvPackage);
        packageLayoutManager = new LinearLayoutManager(WashIronActivity.this);
        packageLayoutManager.scrollToPosition(0);

        //  packageLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        rvPackage.setLayoutManager(packageLayoutManager);
        rvPackage.setHasFixedSize(true);

        if (getIntent() != null) {
            package_category = getIntent().getStringExtra("package_category");
        }

        getPackageList();

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void getPackageList() {

        packageArrayList = new ArrayList<>();
        packageNameArrayList = new ArrayList<>();

        if (Helper.isNetworkAvailable(WashIronActivity.this)) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Helper.showLog("Package List : " + response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            JSONObject result = object.getJSONObject("result");

                            JSONArray package_plan_array = result.getJSONArray("package_plan");
                            for (int j = 0; j < package_plan_array.length(); j++) {

                                JSONObject package_plan_object = package_plan_array.getJSONObject(j);
                                String package_plan_id = package_plan_object.getString("package_plan_id");
                                String package_id = package_plan_object.getString("package_id");
                                String package_plan_person = package_plan_object.getString("package_plan_person");
                                String package_plan_price = package_plan_object.getString("package_plan_price");
                                String package_plan_discount_price = package_plan_object.getString("package_plan_discount_price");
                                String package_plan_final_price = package_plan_object.getString("package_plan_final_price");
                                String package_plan_cloth_limit = package_plan_object.getString("package_plan_cloth_limit");

                                packageList = new PackageList();
                                packageList.setPackage_plan_id(package_plan_id);
                                packageList.setPackage_id(package_id);
                                packageList.setPackage_person(package_plan_person);
                                packageList.setPackage_price(package_plan_price);
                                packageList.setDiscount_price(package_plan_discount_price);
                                packageList.setFinal_price(package_plan_final_price);
                                packageList.setCloth_limit(package_plan_cloth_limit);
                                if (package_category.equals(package_plan_object.getString("package_category").toLowerCase())) {
                                    packageArrayList.add(packageList);

                                }
                            }

                            JSONArray package_name_array = result.getJSONArray("package_name");

                            for (int i = 0; i < package_name_array.length(); i++) {

                                JSONObject package_name_object = package_name_array.getJSONObject(i);
                                String package_id = package_name_object.getString("package_id");
                                String package_name = package_name_object.getString("package_name");

                                packageName = new PackageName();
                                packageName.setPackage_id(package_id);
                                packageName.setPackage_name(package_name);

                                ArrayList<PackageList> packageArrayList1 = new ArrayList<>();

                                for (int k = 0; k < packageArrayList.size(); k++) {
                                    PackageList packageList1 = packageArrayList.get(k);

                                    if (package_id.equalsIgnoreCase(packageList1.getPackage_id())) {
                                        packageArrayList1.add(packageList1);
                                    }
                                }
                                if (packageArrayList1.size()>0){
                                    packageName.setPackageListArray(packageArrayList1);
                                    packageNameArrayList.add(packageName);
                                }
                            }

                            packageAdapter = new PackageNameAdapter(WashIronActivity.this, packageNameArrayList);
                            rvPackage.setAdapter(packageAdapter);

                        } else {
                            Helper.showToast(WashIronActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(WashIronActivity.this, error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("email", "");

            API api = new API(WashIronActivity.this, apiResponse);
            api.execute(1, Services.PACKAGE_LIST, params, true);
        } else {
            Helper.showToast(WashIronActivity.this, Services.NO_NETWORK);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

}
