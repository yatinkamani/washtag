package com.arccus.washtag.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.provider.Settings;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arccus.washtag.R;
import com.arccus.washtag.utils.API;
import com.arccus.washtag.utils.APIResponse;
import com.arccus.washtag.utils.Helper;
import com.arccus.washtag.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.arccus.washtag.utils.Validate.isValidMobile;

public class LoginActivity extends AppCompatActivity {

    private LinearLayout llRegister, llforgot_password, rootView;
    private EditText etEmail, etUserName, etPassword;
    private boolean isPermission = false;
    Animation animMove;
    private TextView tv_country_code;
    private int isPhoneNumber = 0;  // 0 is phone number, 1 is email

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUserName = (EditText) findViewById(R.id.etUserName);
        etPassword = (EditText) findViewById(R.id.etPassword);

        tv_country_code = (TextView) findViewById(R.id.tv_country_code);

        llRegister = (LinearLayout) findViewById(R.id.llRegister);

        llforgot_password = (LinearLayout) findViewById(R.id.llforgot_password);
        rootView = (LinearLayout) findViewById(R.id.rootView);

        animMove = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.move);

        rootView.startAnimation(animMove);

        etUserName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                /*if(isEmail(s.toString())){
                    tv_country_code.setVisibility(View.GONE);
                }*/
                if (isPhone(s.toString())) {
                    isPhoneNumber = 0;
                    tv_country_code.setVisibility(View.VISIBLE);
                } else {
                    isPhoneNumber = 1;
                    tv_country_code.setVisibility(View.GONE);
                }

            }
        });

        llRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        llforgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onForgotPassword();
            }
        });
    }

   /* public static boolean isEmail(String text) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern p = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(text);
        return m.matches();
    }*/

    public static boolean isPhone(String text) {
        if (!TextUtils.isEmpty(text)) {
            return TextUtils.isDigitsOnly(text);
        } else {
            return false;
        }
    }

    public void onLogIn(View view) {

        // String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        String userName = etUserName.getText().toString().trim();

        if (TextUtils.isEmpty(userName)) {
            Helper.showToast(LoginActivity.this, "Enter Email id or phone no");
        } else if (isPhoneNumber == 0 && (!isValidMobile(userName))) {
            Helper.showToast(LoginActivity.this, "Enter valid phone no");
        } else if (isPhoneNumber == 1 && (!Patterns.EMAIL_ADDRESS.matcher(userName).matches())) {
            Helper.showToast(LoginActivity.this, "Enter valid email address");
        } else if (TextUtils.isEmpty(password)) {
            Helper.showToast(LoginActivity.this, "Enter Email id or phone no");
        } else {
            if (Helper.isNetworkAvailable(LoginActivity.this)) {
                APIResponse apiResponse = new APIResponse() {
                    @Override
                    public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                        Helper.showLog("Login : " + response);
                        try {
                            JSONObject object = new JSONObject(response);
                            String status = object.getString("status");
                            String message = object.getString("message");
                            if (status.equalsIgnoreCase("true")) {


                                JSONArray data = object.getJSONArray("result");

                                for (int i = 0; i < data.length(); i++) {

                                    JSONObject result = data.getJSONObject(i);
                                    String user_id = result.getString("user_id");
                                    String user_name = result.getString("user_name");
                                    String email = result.getString("email");
                                    String mobile = result.getString("phone_no");
                                    String locality = result.getString("locality");
                                    String address = result.getString("address");
                                    String latitude = result.getString("latitude");
                                    String longitude = result.getString("longitude");

                                    SharedPreferences sp = getApplicationContext().getSharedPreferences("Laundry", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sp.edit();
                                    editor.putString("user_id", user_id);
                                    editor.putString("user_name", user_name);
                                    editor.putString("email", email);
                                    editor.putString("phone_no", mobile);
                                    editor.putString("locality", locality);
                                    editor.putString("address", address);
                                    editor.putString("latitude", latitude);
                                    editor.putString("longitude", longitude);
                                    editor.apply();

                                    if (latitude.equalsIgnoreCase("null") || TextUtils.isEmpty(latitude) || latitude.equalsIgnoreCase("0.0")) {

                                        Intent intent = new Intent(LoginActivity.this, PlaceActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        finish();

                                    } else {
                                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        finish();
                                    }
                                }

                            } else {
                                Helper.showToast(LoginActivity.this, message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onAPIError(int requestCode, boolean isError, String error) {
                        if (isError) {
                            Helper.showToast(LoginActivity.this, error);
                        }
                    }
                };

                HashMap<String, String> params = new HashMap<>();
                params.put("email", userName);
                params.put("password", password);

                API api = new API(LoginActivity.this, apiResponse);
                api.execute(1, Services.LOGIN, params, true);
            } else {
                Helper.showToast(LoginActivity.this, Services.NO_NETWORK);
            }
        }
    }

    /*private void requestPermission(){

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.RECEIVE_SMS,
                        Manifest.permission.READ_SMS)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {

                            isPermission = true;
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }*/

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    public void onForgotPassword() {

        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
//        LayoutInflater inflater = LoginActivity.this.getLayoutInflater();
        View dialogView = LayoutInflater.from(LoginActivity.this).inflate(R.layout.popup_forgot_password, rootView, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        final EditText etEmail1 = (EditText) dialogView.findViewById(R.id.etEmail);
        TextView tvForgotPassword = (TextView) dialogView.findViewById(R.id.tvForgotPassword);

        final String emptyError = "Enter Email Address or mobile no";
        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = etEmail1.getText().toString().toLowerCase().trim();

                if (TextUtils.isEmpty(email)) {
                    etEmail1.setError(emptyError);
                    etEmail1.requestFocus();
                } else {
                    APIResponse apiResponse = new APIResponse() {
                        @Override
                        public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                            Helper.showLog("Forgot Password : " + response);
                            try {
                                JSONObject object = new JSONObject(response);
                                String status = object.getString("status");
                                String message = object.getString("message");
                                if (status.equalsIgnoreCase("true")) {
                                    Helper.showToast(LoginActivity.this, message);
                                    if (alertDialog.isShowing()) {
                                        alertDialog.dismiss();
                                    }
                                } else {
                                    Helper.showToast(LoginActivity.this, message);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onAPIError(int requestCode, boolean isError, String error) {
                            if (isError) {
                                Helper.showToast(LoginActivity.this, error);
                                if (alertDialog.isShowing()) {
                                    alertDialog.dismiss();
                                }
                            }
                        }
                    };

                    HashMap<String, String> params = new HashMap<>();

                    params.put("email", email);

                    API api = new API(LoginActivity.this, apiResponse);
                    api.execute(1, Services.FORGOT_PASSWORD, params, false);
                }
            }
        });
        alertDialog.show();
    }

}
