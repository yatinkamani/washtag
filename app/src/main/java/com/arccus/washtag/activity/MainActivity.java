package com.arccus.washtag.activity;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;

import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.arccus.washtag.R;
import com.arccus.washtag.fragments.HomeFragment;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private FrameLayout content_frame;
    private NavigationView navigationView;
    private View navHeader;
    private TextView tvUserName;
    private ImageView ivLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);

        mDrawerLayout = findViewById(R.id.drawer_layout);
        content_frame = findViewById(R.id.content_frame);

        navigationView = findViewById(R.id.nav_view);
        navHeader = navigationView.getHeaderView(0);
        tvUserName = (TextView) navHeader.findViewById(R.id.tvuserName);
        ivLogout = (ImageView) navHeader.findViewById(R.id.ivLogout);

        SharedPreferences sp = getApplicationContext().getSharedPreferences("Laundry", Context.MODE_PRIVATE);
        String user_name = sp.getString("user_name", "").trim();
        tvUserName.setText(user_name);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, new HomeFragment())
                .commit();

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persist highlight
//                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        switch (menuItem.getItemId()) {
                            case R.id.nav_home:
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.content_frame, new HomeFragment())
                                        .commit();
                                break;
                            case R.id.nav_profile:
                                Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                               /* ActivityOptions options = ActivityOptions.makeScaleUpAnimation(content_frame, 0,
                                        0, content_frame.getWidth(), content_frame.getHeight());*/
                                startActivity(intent);
                                break;

                            case R.id.nav_my_order:
                                startActivity(new Intent(MainActivity.this, MyOrderActivity.class));
                                break;
                          /*  case R.id.nav_notification:
                               // rateApp();

                                break;*/
                            case R.id.nav_setting:
                                startActivity(new Intent(MainActivity.this, EditProfileActivity.class));
                                break;

                            case R.id.nav_contact_us:

                                Intent intent_contact = new Intent(MainActivity.this, ContactUsActivity.class);
                                startActivity(intent_contact);
                                /*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://kaprat.com/privacypolicy.html"));
                                startActivity(browserIntent);*/
                                break;

                            case R.id.nav_rate_us:
                                launchMarket();
                                break;

                            case R.id.nav_share:
                                shareApp();
                                break;

                            case R.id.nav_logout:
                                logout();
                                break;
                        }
                        return true;
                    }
                });

        ivLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void shareApp() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "Hey check out this app at: https://play.google.com/store/apps/details?id=" + MainActivity.this.getPackageName());
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    private void launchMarket() {
        Uri uri = Uri.parse("market://details?id=" + MainActivity.this.getPackageName());
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(MainActivity.this, " unable to find market app", Toast.LENGTH_LONG).show();
        }
    }

    private void logout() {
        SharedPreferences sp = getApplicationContext().getSharedPreferences("Laundry", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.apply();
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        try {
            if (mDrawerLayout.isDrawerOpen(navigationView))
                mDrawerLayout.closeDrawer(navigationView);
            else
                super.onBackPressed();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
