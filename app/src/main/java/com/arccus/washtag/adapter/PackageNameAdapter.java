package com.arccus.washtag.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arccus.washtag.R;
import com.arccus.washtag.model.PackageList;
import com.arccus.washtag.model.PackageName;

import java.util.ArrayList;

/*
 * Created by KCS on 24-Dec-18.
 * Email : info.kaprat@gmail.com
 */
public class PackageNameAdapter extends RecyclerView.Adapter<PackageNameAdapter.MyViewHolder> {

    private ArrayList<PackageName> packageNameArrayList = new ArrayList<>();
    private Context context;

    public PackageNameAdapter(Context context, ArrayList<PackageName> packageNameArrayList){
        this.context = context;
        this.packageNameArrayList = packageNameArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_package_name,parent,false);
        MyViewHolder vh = new MyViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        PackageName packageName = packageNameArrayList.get(position);

        holder.tv_package_name.setText(packageName.getPackage_name());

        ArrayList<PackageList> packageLists = packageName.getPackageListArray();
        PackageAdapter packageAdapter = new PackageAdapter(context, packageLists);

        holder.rv_package_detail.setHasFixedSize(true);
        holder.rv_package_detail.setLayoutManager(new GridLayoutManager(context,2));
        holder.rv_package_detail.setAdapter(packageAdapter);
    }

    @Override
    public int getItemCount() {
        return packageNameArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_package_name;
        private RecyclerView rv_package_detail;

        public MyViewHolder(View itemView) {
            super(itemView);

            tv_package_name = (TextView) itemView.findViewById(R.id.tv_package_name);
            rv_package_detail = (RecyclerView) itemView.findViewById(R.id.rv_package_detail);

        }
    }
}
