package com.arccus.washtag.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.arccus.washtag.R;
import com.arccus.washtag.helper.ClothItemClickListener;
import com.arccus.washtag.model.ClothDataList;

import java.util.ArrayList;

/**
 * Created by Kaprat on 30-10-2018.
 */

public class ClothAdapter extends RecyclerView.Adapter<ClothAdapter.MyViewHolder> {


    private ArrayList<ClothDataList> clothDataArrayList = new ArrayList<>();
    private ClothItemClickListener clothItemClickListener;
    private static int lastPosition = -1;
    private Context context;

    public ClothAdapter(ArrayList<ClothDataList> clothDataArrayList, ClothItemClickListener clothItemClickListener, Context context){

        this.clothDataArrayList = clothDataArrayList;
        this.clothItemClickListener = clothItemClickListener;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.get_clothes_layout,parent,false);
        MyViewHolder vh = new MyViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return clothDataArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivPlus, ivMinus;
        private TextView tvClothName, tvNumerOfCloth;

        public MyViewHolder(View itemView) {
            super(itemView);

            ivPlus = (ImageView)itemView.findViewById(R.id.ivPlus);
            ivMinus = (ImageView)itemView.findViewById(R.id.ivMinus);
            tvClothName = (TextView)itemView.findViewById(R.id.tvClothName);
            tvNumerOfCloth = (TextView)itemView.findViewById(R.id.tvNumerOfCloth);

        }
    }
}
