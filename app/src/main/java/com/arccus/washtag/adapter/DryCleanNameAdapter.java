package com.arccus.washtag.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arccus.washtag.R;
import com.arccus.washtag.model.DrycleanList;
import com.arccus.washtag.model.DrycleanName;

import java.util.ArrayList;

/*
 * Created by KCS on 25-Dec-18.
 * Email : info.kaprat@gmail.com
 */
public class DryCleanNameAdapter extends RecyclerView.Adapter<DryCleanNameAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<DrycleanName> dryCleanNameArrayList = new ArrayList<>();

    private LinearLayoutManager dryCleanLayoutManager;

    public DryCleanNameAdapter(Context context, ArrayList<DrycleanName> dryCleanNameArrayList){

        this.context = context;
        this.dryCleanNameArrayList = dryCleanNameArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_dryclean_name,parent,false);
        MyViewHolder vh = new MyViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        DrycleanName drycleanName = dryCleanNameArrayList.get(position);

        holder.tv_dryClean_name.setText(drycleanName.getDryclean_name());

        ArrayList<DrycleanList> dryCleanArrayList = drycleanName.getDrycleanListArray();

        dryCleanLayoutManager = new LinearLayoutManager(context);
        dryCleanLayoutManager.scrollToPosition(0);
        holder.rv_dryClean_detail.setLayoutManager(dryCleanLayoutManager);
        holder.rv_dryClean_detail.setHasFixedSize(true);

        DrycleanAdapter drycleanAdapter = new DrycleanAdapter(context, dryCleanArrayList);
        holder.rv_dryClean_detail.setAdapter(drycleanAdapter);
    }

    @Override
    public int getItemCount() {
        return dryCleanNameArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_dryClean_name;
        private RecyclerView rv_dryClean_detail;

        public MyViewHolder(View itemView) {
            super(itemView);

            tv_dryClean_name = (TextView) itemView.findViewById(R.id.tv_dryclean_name);
            rv_dryClean_detail = (RecyclerView) itemView.findViewById(R.id.rv_dryclean_detail);

        }
    }
}
