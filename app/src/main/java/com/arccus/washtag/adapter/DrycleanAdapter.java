package com.arccus.washtag.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arccus.washtag.R;
import com.arccus.washtag.model.DrycleanList;

import java.text.DecimalFormat;
import java.util.ArrayList;

/*
 * Created by KCS on 24-Dec-18.
 * Email : info.kaprat@gmail.com
 */
public class DrycleanAdapter extends RecyclerView.Adapter<DrycleanAdapter.MyViewHolder>{

    DecimalFormat df = new DecimalFormat("0.00");
    private Context context;
    private ArrayList<DrycleanList> drycleanArrayList = new ArrayList<>();

    public DrycleanAdapter(Context context, ArrayList<DrycleanList> drycleanArrayList){
        this.context = context;
        this.drycleanArrayList = drycleanArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_dryclean_price,parent,false);
        MyViewHolder vh = new MyViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final DrycleanList drycleanList = drycleanArrayList.get(position);

        holder.tv_cloth_name.setText(drycleanList.getCloth_name());
        holder.tv_cloth_price.setText(context.getString(R.string.Rs) + df.format(Double.parseDouble(drycleanList.getCloth_price())));

    }

    @Override
    public int getItemCount() {
        return drycleanArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_cloth_name, tv_cloth_price;
        public MyViewHolder(View itemView) {
            super(itemView);

            tv_cloth_name = (TextView) itemView.findViewById(R.id.tv_cloth_name);
            tv_cloth_price = (TextView) itemView.findViewById(R.id.tv_cloth_price);

        }
    }
}
