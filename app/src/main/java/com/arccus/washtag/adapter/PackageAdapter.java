package com.arccus.washtag.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arccus.washtag.R;
import com.arccus.washtag.activity.PackageDetailActivity;
import com.arccus.washtag.model.PackageList;

import java.util.ArrayList;

/*
 * Created by KCS on 20-Nov-18.
 * Email : info.kaprat@gmail.com
 */

public class PackageAdapter extends RecyclerView.Adapter<PackageAdapter.MyViewHolder> {

    private ArrayList<PackageList> packageArrayList;
    private Context context;


    public PackageAdapter(Context context, ArrayList<PackageList> packageArrayList) {
        this.context = context;
        this.packageArrayList = packageArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.get_package_list_layout, parent, false);
        MyViewHolder vh = new MyViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final PackageList packageList = packageArrayList.get(position);

        holder.tv_cloth_limit.setText("Cloth limit " + packageList.getCloth_limit());
        holder.tv_plan_price.setText(packageList.getPackage_price() + "/-");
        holder.tv_final_price.setText(packageList.getFinal_price() + "/-");

        if (packageList.getDiscount_price().equals("0")){
            holder.tv_text.setVisibility(View.GONE);
            holder.tv_final_price.setVisibility(View.GONE);
            holder.tv_discount_price.setVisibility(View.GONE);
        }else {
            holder.tv_discount_price.setVisibility(View.VISIBLE);
            holder.tv_final_price.setVisibility(View.VISIBLE);
            holder.tv_text.setVisibility(View.VISIBLE);
            holder.tv_discount_price.setText("Dis. " + packageList.getDiscount_price() + "/-");
        }

        if ((position % 2) == 0) {

            holder.llPackage.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
            holder.tv_cloth_limit.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            holder.tv_plan_price.setTextColor(ContextCompat.getColor(context, R.color.black));
            holder.tv_text.setTextColor(ContextCompat.getColor(context, R.color.black));
            holder.tv_final_price.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            holder.tv_discount_price.setTextColor(ContextCompat.getColor(context, R.color.black));

        } else {
            holder.llPackage.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
            holder.tv_cloth_limit.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
            holder.tv_plan_price.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.tv_text.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.tv_final_price.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
            holder.tv_discount_price.setTextColor(ContextCompat.getColor(context, R.color.white));
        }

        holder.llPackage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, PackageDetailActivity.class);
                intent.putExtra("PackagePlanId", packageList.getPackage_plan_id());
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return packageArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_cloth_limit, tv_plan_price, tv_final_price, tv_discount_price, tv_text;
        LinearLayout llPackage;

        MyViewHolder(View itemView) {
            super(itemView);

            llPackage = (LinearLayout) itemView.findViewById(R.id.llPackage);
            tv_cloth_limit = (TextView) itemView.findViewById(R.id.tv_cloth_limit);
            tv_plan_price = (TextView) itemView.findViewById(R.id.tv_plan_price);
            tv_final_price = (TextView) itemView.findViewById(R.id.tv_final_price);
            tv_discount_price = (TextView) itemView.findViewById(R.id.tv_discount_price);
            tv_text = (TextView) itemView.findViewById(R.id.tv_text);
        }
    }
}
