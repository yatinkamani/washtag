package com.arccus.washtag.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arccus.washtag.R;
import com.arccus.washtag.model.OrderedClothList;

import java.util.ArrayList;

/*
 * Created by KCS on 22-Nov-18.
 * Email : info.kaprat@gmail.com
 */
public class OrderedClothAdapter extends RecyclerView.Adapter<OrderedClothAdapter.MyViewHolder> {

    private ArrayList<OrderedClothList> orderedClothListArrayList = new ArrayList<>();
    private Context context;

    public  OrderedClothAdapter(Context context, ArrayList<OrderedClothList> orderedClothListArrayList){
        this.context = context;
        this.orderedClothListArrayList = orderedClothListArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ordered_cloth_row,parent,false);
        MyViewHolder vh = new MyViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        OrderedClothList orderedClothList = orderedClothListArrayList.get(position);

        holder.tv_quantity.setText(orderedClothList.getCloth_quantity());
        holder.tv_cloth_name.setText(orderedClothList.getCloth_name());

    }

    @Override
    public int getItemCount() {
        return orderedClothListArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_quantity, tv_cloth_name;

        public MyViewHolder(View itemView) {
            super(itemView);

            tv_quantity = (TextView) itemView.findViewById(R.id.tv_quantity);
            tv_cloth_name = (TextView) itemView.findViewById(R.id.tv_cloth_name);

        }
    }
}
