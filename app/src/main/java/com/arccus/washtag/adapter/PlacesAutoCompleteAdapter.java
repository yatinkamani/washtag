package com.arccus.washtag.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.arccus.washtag.utils.PlaceDetailsResponse;
import com.arccus.washtag.utils.Services;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse;
import com.google.android.libraries.places.api.net.PlacesClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class PlacesAutoCompleteAdapter extends ArrayAdapter<PlacesAutoCompleteAdapter.PlaceAutocomplete> implements Filterable {

    private static final String TAG = "PlacesAutoCompleteAdapter";
    private ArrayList<PlaceAutocomplete> mResultList;
    private GoogleApiClient mGoogleApiClient;
    private LatLngBounds mBounds;
    private AutocompleteFilter mPlaceFilter;
    private PlacesClient placesClient;
    public PlacesAutoCompleteAdapter(Context context, int resource, GoogleApiClient googleApiClient, LatLngBounds bounds, AutocompleteFilter filter) {
        super(context, resource);
        mGoogleApiClient = googleApiClient;
        mBounds = bounds;
        mPlaceFilter = filter;
        Places.initialize(context,Services.api_key);
        placesClient = Places.createClient(context);
    }

    public static Map getPlaceData(Context context, String placeId, final PlaceDetailsResponse placeListener) {
        final HashMap<String, String> map = new HashMap<>();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyDLXMcG3u8pfDEvGLiVrxInWaFFaC0_jUo&placeid=" + placeId, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    JSONObject result = object.getJSONObject("result");
                    JSONArray address_components = result.getJSONArray("address_components");
                    JSONObject cityObj = address_components.getJSONObject(0);
                    String long_city_name = cityObj.getString("long_name");
                    String short_city_name = cityObj.getString("short_name");
                    map.put("long_city_name", long_city_name);
                    map.put("short_city_name", short_city_name);

                    JSONObject stateObj = address_components.getJSONObject(2);
                    String long_state_name = stateObj.getString("long_name");
                    String short_state_name = stateObj.getString("short_name");
                    map.put("long_state_name", long_state_name);
                    map.put("short_state_name", short_state_name);

                    JSONObject countryObj = address_components.getJSONObject(3);
                    String long_country_name = countryObj.getString("long_name");
                    String short_country_name = countryObj.getString("short_name");
                    map.put("long_country_name", long_country_name);
                    map.put("short_country_name", short_country_name);

                    if (placeListener != null) {
                        placeListener.onPlaceDetailSuccess(true, map);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    if (placeListener != null) {
                        placeListener.onPlaceDetailSuccess(false, map);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (placeListener != null) {
                    placeListener.onPlaceDetailSuccess(false, map);
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
        return map;
    }

    public void setBounds(LatLngBounds bounds) {
        mBounds = bounds;
    }

    @Override
    public int getCount() {
        return mResultList != null ? mResultList.size() : 0;
    }

    @Override
    public PlaceAutocomplete getItem(int position) {
        return mResultList.get(position);
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                if (constraint != null) {
                    mResultList = getAutocomplete(constraint);
                    if (mResultList != null) {
                        results.values = mResultList;
                        results.count = mResultList.size();
                    }
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged(); // The API returned at least one result, update the data.
                } else {
                    notifyDataSetInvalidated(); // The API did not return any results, invalidate the data set.
                }
            }
        };
        return filter;
    }

    @SuppressLint("LongLogTag")
    private ArrayList<PlaceAutocomplete> getAutocomplete(CharSequence constraint) {
        if (mGoogleApiClient.isConnected()) {
            //             Log.e(TAG, "Google API client is connected");
            //            PendingResult<AutocompletePredictionBuffer> results = Places.GeoDataApi.getAutocompletePredictions(mGoogleApiClient, constraint.toString(), mBounds, mPlaceFilter);
            //            AutocompletePredictionBuffer autocompletePredictions = results.await(60, TimeUnit.SECONDS);

            AutocompleteSessionToken token = AutocompleteSessionToken.newInstance();
            RectangularBounds bounds = RectangularBounds.newInstance(mBounds.southwest,mBounds.northeast);
            FindAutocompletePredictionsRequest request = FindAutocompletePredictionsRequest.builder()
                          // Call either setLocationBias() OR setLocationRestriction().
                    .setLocationBias(bounds)
//                    .setLocationRestriction(mBounds)
//                    .setCountry("ind")
//                    .setTypeFilter(TypeFilter.ADDRESS)
                    .setSessionToken(token)
                    .setQuery(""+constraint)
                    .build();

            Task<FindAutocompletePredictionsResponse> autocompletePredictions = placesClient.findAutocompletePredictions(request);

            /*final Status status = autocompletePredictions.getStatus();
            if (!status.isSuccess()) {
                Log.e(TAG, "Error : " + status);
                autocompletePredictions.release();
                return null;
            }*/
            try {
                Tasks.await(autocompletePredictions, 60, TimeUnit.SECONDS);
            } catch (ExecutionException | InterruptedException | TimeoutException e) {
                e.printStackTrace();
            }

//            Iterator<AutocompletePrediction> iterator = autocompletePredictions.iterator();
            ArrayList resultList = new ArrayList<>();
            if (autocompletePredictions.isSuccessful()) {
                FindAutocompletePredictionsResponse findAutocompletePredictionsResponse = autocompletePredictions.getResult();
                if (findAutocompletePredictionsResponse != null)
                    for (com.google.android.libraries.places.api.model.AutocompletePrediction prediction : findAutocompletePredictionsResponse.getAutocompletePredictions()) {
                        Log.i(TAG, prediction.getPlaceId());
                        Log.i(TAG, prediction.getPrimaryText(null).toString());

                        resultList.add(new PlaceAutocomplete(prediction.getPlaceId(), prediction.getFullText(null).toString()));

                    }

                return resultList;
            } else {
                return resultList;
            }
           /* while (iterator.hasNext()) {
                AutocompletePrediction prediction = iterator.next();
                final CharacterStyle STYLE_BOLD = new StyleSpan(Typeface.BOLD);
                //https://maps.googleapis.com/maps/api/place/autocomplete/json?key=AIzaSyDxaYyTAzPQZGH51umbLhS06UOZL4YpfPU&components=country:in&input=rajkot
                //Log.e("Prediction","Place id : "+prediction.getPlaceId()+" Full Text : "+prediction.getFullText(null)+" Place Type : "+prediction.getPlaceTypes()+" Primary Text : "+prediction.getPrimaryText(null)+" Secondary Text : "+prediction.getSecondaryText(null));
                //E/Prediction: Place id : ChIJD98cx4rJWTkRO62Tvs8V3XY Full Text : Rajkot, Gujarat, India Place Type : [1009, 1012, 1007] Primary Text : Rajkot Secondary Text : Gujarat, India
                resultList.add(new PlaceAutocomplete(prediction.getPlaceId(), prediction.getFullText(null)));
                //To get city name and postal code from Google Place API on Android
                //https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyDxaYyTAzPQZGH51umbLhS06UOZL4YpfPU&placeid=ChIJD98cx4rJWTkRO62Tvs8V3XY
                //https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDxaYyTAzPQZGH51umbLhS06UOZL4YpfPU&latlng=22.2906835,70.785403&sensor=true
            }*/

//            autocompletePredictions.release();
        }
        Log.e(TAG, "Google API client is not connected.");
        return null;
    }

    public class PlaceAutocomplete {
        public CharSequence placeId;
        public CharSequence description;

        PlaceAutocomplete(CharSequence placeId, CharSequence description) {
            this.placeId = placeId;
            this.description = description;
        }

        @Override
        public String toString() {
            return description.toString();
        }
    }
}